#!/usr/bin/env python

import sys
import os
import nibabel as nib
import numpy as np

# 1: Filename of the reference to create the mask

img = nib.load(sys.argv[1]+'/fa.nii.gz')
data = img.get_data()

new_volume_left = np.zeros((data.shape))
new_volume_left[0:data.shape[0]/2:,:] = 1
nib.save(nib.Nifti1Image(new_volume_left, img.affine), sys.argv[1]+'/left.nii')

new_volume_rigth = np.zeros((data.shape))
new_volume_rigth[data.shape[0]/2:data.shape[0]-1:,:] = 1
nib.save(nib.Nifti1Image(new_volume_rigth, img.affine), sys.argv[1]+'/rigth.nii')
