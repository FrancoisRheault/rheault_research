#!/usr/bin/env python

import sys
import os
from openpyxl import load_workbook
import json

# 1: excel_filename
# 2: encryption_filename

def rename_excel_cell(excel_filename, encryption_filename, bundle_length):
    wb = load_workbook(excel_filename, read_only=False)
    ws = wb["metadata"]
    with open(encryption_filename) as f:
        encrypt_dict = json.load(f)
    list_directory = sorted(encrypt_dict.values())
    dict_all_value = {}
    for i in range(len(list_directory)):
        for j in range(int(bundle_length)):
            for key in encrypt_dict:
                column = str(i * int(bundle_length) + j + 6)
                if ws['b'+column].value == key:
                    ws['b'+column] = encrypt_dict[key]
                if ws['b'+column].value == encrypt_dict[key]:
                    ws['b'+column] = key
                else:
                    continue

            dict_value = {}
            subject_name = ws['b'+column].value+'_'+ws['c'+column].value

            if ws['d'+column].value is None:
                dict_value['date'] = None
            else:
                dict_value['date'] = str(ws['d'+column].value)

            if ws['d'+column].value is None:
                dict_value['start_time'] = None
            else:
                dict_value['start_time'] = str(ws['e' + column].value)

            if ws['d'+column].value is None:
                dict_value['duration'] = None
            else:
                dict_value['duration'] = str(ws['f'+column].value)

            if ws['d'+column].value is None:
                dict_value['software'] = None
            else:
                dict_value['software'] = str(ws['g'+column].value)

            dict_all_value[subject_name] = dict_value

    out_name = os.path.splitext(os.path.basename(excel_filename))[0]
    with open(out_name+'_decrypt.json', 'w') as outfile:
        json.dump(dict_all_value, outfile)
    base, ext = os.path.splitext(excel_filename)
    wb.save(base+'_decrypt'+ext)

def main():
    if os.path.splitext(os.path.basename(sys.argv[1]))[0] == os.path.splitext(os.path.basename(sys.argv[2]))[0]:
        rename_excel_cell(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print "Decryption file should have the same name as the excel file"


if __name__ == "__main__":
    main()
