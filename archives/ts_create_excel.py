#!/usr/bin/env python

import sys
import os
from openpyxl import load_workbook
from copy import copy
import json

# 1: Path to the 'mockup_metadata_sheet.xlsx'
# 2: Name of the folder
# 3: List of bundle name
# 4: Output name


def main():
    wb = load_workbook(sys.argv[1])
    ws = wb["metadata"]
    with open(sys.argv[2]) as f:
        encrypt_dict = json.load(f)
    list_directory = sorted(encrypt_dict.values())
    list_bundle = sys.argv[3:len(sys.argv)-1]
    for row in 'bcdefgh':
        for i in range(len(list_directory)):
            for j in range(len(list_bundle)):
                column = str(i * len(list_bundle) + j + 6)
                ws[row+column]._style = copy(ws[row+'6']._style)
                if row == 'b':
                    ws[row+column].value = list_directory[i]
                elif row == 'c':
                    ws[row+column].value = list_bundle[j]
    wb.save(sys.argv[len(sys.argv)-1])

if __name__ == "__main__":
    main()

