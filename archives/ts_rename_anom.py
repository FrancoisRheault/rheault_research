#!/usr/bin/env python

import sys
import os
import numpy as np
import hashlib
import random
import uuid
import json

# In a folder of an expert, launch with the args. "generate"
# Then relaunch with the new file expert_name.txt

def encrypt(directory):
    name_list = []
    rename_list = []
    letter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for sub in os.listdir(directory):
        if not os.path.isdir(os.path.join(directory, sub)):
            continue
        if sub[0] in letter and sub[1:4].isdigit and len(sub) == 4:
            raise ValueError('Very likely already encrypted')
        name_list.append(sub)

    name_list = sorted(name_list)

    it = 0
    while it < len(name_list):
        val = str(int(uuid.uuid4()))
        new_name = letter[int(val[0:10]) % 26] + '{0}'.format(str(int(val[10:25]) % 999).zfill(3))
        if new_name not in rename_list:
            rename_list.append(new_name)
            it += 1

    encrypt_dict = dict(zip(name_list, rename_list))
    out_name = os.path.basename(os.path.normpath(directory))
    with open(out_name+'.json', 'w') as outfile:
        json.dump(encrypt_dict, outfile)


def rename_using_encryption(directory, encryption_filename):
    with open(encryption_filename) as f:
        names = json.load(f)
    for key in names:
        n0 = os.path.join(os.path.abspath(directory), key)
        n1 = os.path.join(os.path.abspath(directory), names[key])
        if  not os.path.isdir(n0) and not os.path.isdir(n1):
            print('No matching folder, wrong parent')
            break

    for key in names:
        n0 = os.path.join(os.path.abspath(directory), key)
        n1 = os.path.join(os.path.abspath(directory), names[key])
        if os.path.isdir(n0):
            os.rename(n0, n1)
        elif os.path.isdir(n1):
            os.rename(n1, n0)


def main():
    if sys.argv[1] == 'encrypt':
        encrypt(sys.argv[2])
        dict_name = os.path.basename(os.path.normpath(sys.argv[2]))
        rename_using_encryption(sys.argv[2], dict_name+'.json')
    elif sys.argv[1] == 'decrypt':
        rename_using_encryption(sys.argv[2], sys.argv[3])
    else:
        print('Wrong option!')


if __name__ == "__main__":
    main()
