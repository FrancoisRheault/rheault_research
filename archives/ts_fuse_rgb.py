#!/usr/bin/env python

import nibabel as nib
import numpy as np
import os
import sys

from scilpy.utils.filenames import split_name_with_nii

# 1: Filename of the original RGB

shape = nib.load(sys.argv[1]).get_shape()
shape_4d = (shape[0], shape[1], shape[2], 3)
new_data = np.zeros(shape_4d)

name = ['_r','_g','_b']
basename, ext = split_name_with_nii(sys.argv[1])
for i in range(3):
    img = nib.load(basename+name[i]+ext)
    data = img.get_data()
    new_data[..., i] = data

nib.save(nib.Nifti1Image(new_data.astype(np.uint8), img.affine), basename+'_fuse'+ext)
