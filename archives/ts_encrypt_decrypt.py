#!/usr/bin/env python

import sys
import os
import numpy as np
import hashlib
import random
import uuid
import json

# In a folder of an expert, launch with the args. "generate"
# Then relaunch with the new file expert_name.txt
def find_existing_encryption(basedir, sub, encrypt_dict):
    for key in encrypt_dict:
        n0 = os.path.join(os.path.abspath(basedir), key)
        n1 = os.path.join(os.path.abspath(basedir), encrypt_dict[key])
        if not key == sub and not encrypt_dict[key] == sub:
            continue
        if os.path.isdir(n0):
            return key, encrypt_dict[key]
        elif os.path.isdir(n1):
            return encrypt_dict[key], key
    return None


def rename_using_encryption(basedir, participant, sub):
    encryption_filename = participant+'.json'
    if os.path.isfile(encryption_filename):
        with open(encryption_filename) as f:
            encrypt_dict = json.load(f)
        tuple_in_out = find_existing_encryption(basedir, sub, encrypt_dict)
    else:
        tuple_in_out = None
        encrypt_dict = {}

    if tuple_in_out:
        in_name, out_name = tuple_in_out
    else:
        in_name = sub
        letter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        if not os.path.isdir(os.path.join(basedir, sub)):
            raise ValueError('Folder does not exist')
        val = str(int(abs(hash(participant+sub)) % (10 ** 16)))
        out_name = letter[int(val[0:10]) % 26] + '{0}'.format(str(int(val[10:25]) % 999).zfill(3))
    
        encrypt_dict[sub] = out_name
        with open(encryption_filename, 'w') as outfile:
            json.dump(encrypt_dict, outfile)

    in_dir = os.path.join(basedir, in_name)
    out_dir = os.path.join(basedir, out_name)
    os.rename(in_dir, out_dir)
    

def main():
    path = os.path.abspath(sys.argv[1])
    hierarchy = path.split(os.sep)
    basedir = '/'+os.path.join(*hierarchy[0:len(hierarchy)-1])
    subfolder_name = hierarchy[-1]

    participant = sys.argv[2]
    rename_using_encryption(basedir, participant, subfolder_name)


if __name__ == "__main__":
    main()
