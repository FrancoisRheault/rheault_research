#!/usr/bin/env python

import nibabel as nib
import numpy as np
import sys

# 1:Path of the image to patch

img = nib.load(sys.argv[1])
data = img.get_data()

patching_size = 8
new_volume = np.zeros((data.shape[0]+patching_size,
                       data.shape[1]+patching_size,
                       data.shape[2]+patching_size))

new_volume[patching_size:, patching_size:, patching_size:] = data
nib.save(nib.Nifti1Image(new_volume.astype(np.float32), img.affine), sys.argv[2])
