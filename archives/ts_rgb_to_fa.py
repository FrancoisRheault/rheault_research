#!/usr/bin/env python

import sys
import nibabel as nib
import numpy as np

# 1: Path of the RGB
# 2: Path of the FA

img = nib.load(sys.argv[1])
data = img.get_data()
data.shape
fa = np.linalg.norm(data, axis=3).astype(np.float32)/255.0
nib.save(nib.Nifti1Image(fa, img.affine), sys.argv[2])
