#!/usr/bin/env python
import vtk
import numpy as np
import sys

transform = vtk.vtkTransform()
transform.RotateWXYZ(5, 1, 0, 1)
transform.RotateWXYZ(2, 0, 1, 0)

matrix = np.eye(4)
for i in range(4):
    for j in range(4):
        matrix[i, j] = transform.GetMatrix().GetElement(i, j)
print matrix
np.savetxt('transformation_rigid.txt', np.linalg.inv(matrix))
