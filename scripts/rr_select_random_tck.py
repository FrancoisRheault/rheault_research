import os.path as op
import numpy as np
from time import time
import os
import sys
import nibabel as nib
from nibabel.streamlines import LazyTractogram, detect_format


#from nipype/interfaces/mrtrix/convert.py
def read_mrtrix_header(in_file):
    fileobj = open(in_file, "rb")
    header = {}
    #iflogger.info("Reading header data...")
    for line in fileobj:
        line = line.decode()
        if line == "END\n":
            #iflogger.info("Reached the end of the header!")
            break
        elif ": " in line:
            line = line.replace("\n", "")
            line = line.replace("'", "")
            key = line.split(": ")[0]
            value = line.split(": ")[1]
            header[key] = value
            #iflogger.info('...adding "%s" to header for key "%s"', value, key)
    fileobj.close()
    header["count"] = int(header["count"].replace("\n", ""))
    header["offset"] = int(header["file"].replace(".", ""))
    return header

#from nipype/interfaces/mrtrix/convert.py
def read_mrtrix_streamlines(in_file, header):
    byte_offset = header["offset"]
    stream_count = header["count"]
    datatype = header["datatype"]
    dt = 4
    if datatype.startswith( 'Float64' ):
        dt = 8
    elif not datatype.startswith( 'Float32' ):
        print('Unsupported datatype: ' + datatype)
        return
    #tck format stores three floats (x/y/z) for each vertex
    num_triplets = (os.path.getsize(in_file) - byte_offset) // (dt * 3)
    dt = 'f' + str(dt)
    if datatype.endswith( 'LE' ):
        dt = '<'+dt    
    if datatype.endswith( 'BE' ):
        dt = '>'+dt
    vtx = np.memmap(in_file, dtype=dt, shape=(num_triplets*3), offset=byte_offset)
    vtx = np.reshape(vtx, (-1,3)) 
    #make sure last streamline delimited...
    if not np.isnan(vtx[-2,1]):
        vtx[-1,:] = np.nan
    line_ends, = np.where(np.all(np.isnan(vtx), axis=1));
    if stream_count != line_ends.size:
        print('expected {} streamlines, found {}'.format(stream_count, line_ends.size))
    line_starts = line_ends + 0
    line_starts[1:line_ends.size] = line_ends[0:line_ends.size-1]
    #the first line starts with the first vertex (index 0), so preceding NaN at -1
    line_starts[0] = -1;
    #first vertex of line is the one after a NaN
    line_starts = line_starts + 1
    #last vertex of line is the one before NaN
    line_ends = line_ends - 1
    return vtx, line_starts, line_ends


#from nipype/interfaces/mrtrix/convert.py
def read_mrtrix_tracks(in_file):
    header = read_mrtrix_header(in_file)
    vertices, line_starts, line_ends = read_mrtrix_streamlines(in_file, header)
    return header, vertices, line_starts, line_ends

def generator_from_memmaps(vtx, line_starts, line_ends, indices):
    for i in indices:
        s = vtx[line_starts[i]:line_ends[i]]
        yield s

if __name__ == '__main__':
    # Parser
    in_filename = sys.argv[1]
    nbr_to_select = int(sys.argv[2])
    random_seed = int(sys.argv[3])
    out_filename = sys.argv[4]

    # Generate the list of random indices
    nbr_streamlines = read_mrtrix_header(in_filename)['count']
    np.random.seed(random_seed)
    indices = np.sort(np.random.uniform(low=0, high=nbr_streamlines, size=(nbr_to_select,)).astype(np.int64))

    # Read as memmap
    start = time()
    _, vertices, line_starts, line_ends = read_mrtrix_tracks(in_filename)

    # Lazy saving with nibabel (still slow for big one, but no RAM limit)
    generator = generator_from_memmaps(vertices, line_starts, line_ends, indices)
    out_tractogram = LazyTractogram(lambda: generator,
                                    affine_to_rasmm=np.eye(4))
    tractogram_type = detect_format(out_filename)
    new_header = tractogram_type.create_empty_header()
    nib.streamlines.save(out_tractogram, out_filename, header=new_header)

    print('Number of streamlines: {} with {} vertices'.format(line_ends.size, vertices.shape[0]))
    print('Selecting {} random streamlines (seed #{})'.format(nbr_to_select, random_seed))
    print('{} Operation performed in {} seconds'.format(in_filename, np.round(time()-start, 3)))

 