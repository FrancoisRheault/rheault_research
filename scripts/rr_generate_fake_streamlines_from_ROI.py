#!/usr/bin/env python
import argparse
import os
import random

import nibabel as nib
import numpy as np
from scipy.ndimage.morphology import binary_erosion

from dipy.tracking.streamline import set_number_of_points, transform_streamlines, length
from scilpy.io.utils import read_info_from_mb_bdo
from dipy.io.utils import create_tractogram_header, get_reference_info
from scilpy.utils.filenames import split_name_with_nii

from rheault_research.utils.streamlines import smooth_line_gaussian, smooth_line_spline
DESCRIPTION = """
    Generate a smooth set of streamlines between ROI (in order). The ROI
    can be a mix of *.bdo or *.nii.gz.
    WARNING: A spline is used, so the path can bet outside of the ROI,
    so no sharp turn...
    """


# def smooth_line_spline(streamline, sigma, error_rate):
#     if sigma < 0.001:
#         ValueError('Cant have a 0 sigma with spline')

#     nb_points = int(length(streamline)*1)
#     if nb_points < 2:
#         nb_points = 2
#     tck, u = splprep(streamline.T, s=sigma)
#     smoothed_streamlines = splev(np.linspace(0, 1, nb_points), tck)
#     smoothed_streamlines = np.squeeze(np.asarray([smoothed_streamlines]).T)

#     # Ensure first and last point remain the same
#     smoothed_streamlines[0] = streamline[0]
#     smoothed_streamlines[len(smoothed_streamlines)-1] = \
#         streamline[len(streamline)-1]

#     if error_rate > 0.0001:
#         return compress_streamlines(smoothed_streamlines,
#                                     error_rate)
#     else:
#         return set_number_of_points(smoothed_streamlines, 99)

def buildArgsParser():

    p = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=DESCRIPTION)

    p.add_argument('ref_anat',
                   help='The reference image that fit the ROI')

    p.add_argument('ROI', nargs='+',
                   help='List of ROI, in order, that create a path, \n'
                        'it can be *.bdo or *.nii.gz')

    p.add_argument('output',
                   help='Output filename (*.trk)')

    p.add_argument('--nbr_streamlines', type=int, default=15000,
                   help='Desired number of streamlines')

    p.add_argument('--sigma', type=float, default=0.1,
                   help='Change the spline sigma, impact curvature')

    p.add_argument('--ctrl_points', type=int,  default=5,
                   help='Change the number of control points, impact curvature')

    p.add_argument('--seed', type=int, default=42,
                   help='Change the random number generator seed')

    p.add_argument('--noise_factor', type=int, default=5,
                   help='Value between 1-10, add noise relative to voxel size')

    p.add_argument('-f', action='store_true', dest='force_overwrite',
                   help='force (overwrite output file if present)')

    return p


def sphere_sampling(so_filename, nbr_pts):
    # Read the *.bdo file
    _, radius, center = read_info_from_mb_bdo(so_filename)

    # Generate a random set of polar coordinate
    u = np.random.rand(nbr_pts)
    phi = np.random.rand(nbr_pts) * 2 * 3.1415926
    theta = np.arccos(np.random.rand(nbr_pts) * 2 - 1)
    # Generate the points a bit within the sphere
    x = u*(radius[0] - 1) * np.sin(theta) * np.cos(phi) + center[0]
    y = u*(radius[1] - 1) * np.sin(theta) * np.sin(phi) + center[1]
    z = u*(radius[2] - 1) * np.cos(theta) + center[2]

    return np.array([x, y, z]).T


def mask_sampling(mask_filename, nbr_pts):
    # Load the *.nii file
    img = nib.load(mask_filename)
    data = img.get_data()
    # Erosion, to make sure the points are just within the mask
    #data = binary_erosion(data)

    # Get all voxel, bring them to world space
    indices = np.argwhere(data > 0)
    current_sampling = transform_streamlines(list(indices), img.affine)

    # For the sample function, we need a list just as long
    new_sampling = []
    if len(current_sampling) < nbr_pts:
        for i in range(int((nbr_pts / len(current_sampling))+1)):
            new_sampling.extend(current_sampling)
        return np.asarray(random.sample(new_sampling, nbr_pts))
    else:
        return np.asarray(random.sample(current_sampling, nbr_pts))


def main():
    parser = buildArgsParser()
    args = parser.parse_args()

    if not os.path.isfile(args.ref_anat):
        parser.error('"{0}" must be a file!'.format(args.ref_anat))

    for filename in args.ROI:
        if not os.path.isfile(filename):
            parser.error('"{0}" must be a file!'.format(filename))

    if os.path.isfile(args.output) and not args.force_overwrite:
        parser.error('"{0}" already exists! Use -f to overwrite it.'
                     .format(args.output))

    nbr_streamlines = args.nbr_streamlines
    np.random.seed(args.seed)

    # For each ROI we append a set of $nbr_streamlines points
    sampling = []
    for filename in args.ROI:
        _, ext = split_name_with_nii(filename)
        if ext == '.bdo':
            sampling.append(sphere_sampling(filename, nbr_streamlines))
        elif ext == '.nii' or ext == '.nii.gz':
            sampling.append(mask_sampling(filename, nbr_streamlines))
    # A streamline will go through a random point in each ROI
    network = []
    for i in range(nbr_streamlines):

        pos = np.random.randint(0, nbr_streamlines)
        streamline = sampling[0][pos]
        for j in range(1, len(args.ROI)):
            pos = np.random.randint(0, nbr_streamlines)
            streamline = np.vstack((streamline, sampling[j][pos]))

        # Minimal amount of control points for the spline
        streamline = set_number_of_points(streamline, min(args.ctrl_points,
                                                          len(args.ROI)))
        smooth_streamlines = smooth_line_gaussian(streamline, args.sigma, 1)

        # Noise can be nice (or not) for visualisation
        noise = np.random.random((len(smooth_streamlines), 3))/0.1
        network.append(smooth_streamlines + noise)

    # Creation of an *.trk using the provided reference
    new_tractogram = nib.streamlines.Tractogram(network,
                                                affine_to_rasmm=np.eye(4))
    info = get_reference_info(args.ref_anat)
    header = create_tractogram_header(args.output, *info)
    trkfile = nib.streamlines.TrkFile(new_tractogram, header=header)
    nib.streamlines.save(trkfile, args.output)

if __name__ == "__main__":
    main()
