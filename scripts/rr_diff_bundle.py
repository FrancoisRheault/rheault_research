#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from operator import itemgetter
import os

import nibabel as nib
import numpy as np
import pickle
from dipy.viz import window, actor
from random import shuffle
from scilpy.tractanalysis import compute_robust_tract_counts_map
from scilpy.io.streamlines import load_trk_in_voxel_space
from scipy.ndimage import gaussian_filter

try:
    from dipy.segment.clustering import qbx_and_merge
except ImportError as e:
    raise ValueError('QBx is not available, switch Dipy branch')

DESCRIPTION = """
    This script helps using QuickBundlesX, need a special branch.
    We cannot know the number of clusters in advance, also the tracking
    algorithm (DTI vs Det. vs Prob.) makes it even harder.
"""


def buildArgsParser():
    p = argparse.ArgumentParser(description=DESCRIPTION,
                                formatter_class=argparse.RawTextHelpFormatter)

    p.add_argument('tractogram_1',
                   help='Tractogram filename. Format must be readable'
                   ' by the Nibabel.')
    p.add_argument('tractogram_2',
                   help='Tractogram filename. Format must be readable'
                   ' by the Nibabel.')

    p.add_argument('--window', type=int, default=5,
                   help='Quickbundles threshold in mm.')
    p.add_argument('--output_corr', default='corr.nii.gz',
                   help='Filename')
    p.add_argument('--output_diff', default='diff.nii.gz',
                   help='Filename')
    p.add_argument('-f', action='store_true', dest='force_overwrite',
                   help='force (overwrite output file if present)')

    return p


def main():
    parser = buildArgsParser()
    args = parser.parse_args()

    if not os.path.isfile(args.tractogram_1):
        parser.error('"{0}" must be a file!'.format(args.tractogram_1))
    if not os.path.isfile(args.tractogram_2):
        parser.error('"{0}" must be a file!'.format(args.tractogram_2))
    if os.path.exists(args.output_corr) and not args.force_overwrite:
        parser.error('"{0}" is already a file, use -f to enable the overwrite'.
                     format(args.output_corr))
    if os.path.exists(args.output_diff) and not args.force_overwrite:
        parser.error('"{0}" is already a file, use -f to enable the overwrite'.
                     format(args.output_diff))

    trk_file = trk_file = nib.streamlines.load(
        args.tractogram_1, lazy_load=True)
    dim = trk_file.header[nib.streamlines.Field.DIMENSIONS]
    affine = trk_file.header[nib.streamlines.Field.VOXEL_TO_RASMM]

    streamlines_1 = load_trk_in_voxel_space(args.tractogram_1)
    streamlines_2 = load_trk_in_voxel_space(args.tractogram_2)
    data_1 = compute_robust_tract_counts_map(streamlines_1, dim)
    data_2 = compute_robust_tract_counts_map(streamlines_2, dim)

    mask = np.zeros(dim)
    corr = np.zeros(dim)
    mask_diff = np.zeros(dim)
    mask[data_1+data_2 > 0] = 1
    mask_diff[data_1+data_2 == 1] = 1

    indices = np.where(mask > 0)
    data_1[mask == 0] = np.random.normal(0., 1., len(data_1[mask == 0]))
    data_2[mask == 0] = np.random.normal(0., 1., len(data_2[mask == 0]))
    for i in range(len(indices[0])):
        ind = (indices[0][i], indices[1][i], indices[2][i])
        min_x, max_x = max(0, ind[0] - args.window), min(mask.shape[0], ind[0] + args.window + 1)
        min_y, max_y = max(0, ind[1] - args.window), min(mask.shape[1], ind[1] + args.window + 1)
        min_z, max_z = max(0, ind[2] - args.window), min(mask.shape[2], ind[2] + args.window + 1)

        window_1 = data_1[min_x:max_x, min_y:max_y, min_z:max_z]
        window_2 = data_2[min_x:max_x, min_y:max_y, min_z:max_z]
        mask_window = mask[min_x:max_x, min_y:max_y, min_z:max_z]

        if np.sum(window_1[mask_window > 0]) < 1 \
            or np.sum(window_2[mask_window > 0]) < 1 \
            or np.sum(mask_window) < args.window**3 / 3:
            continue
        coeff = np.corrcoef(window_1[mask_window > 0],
                            window_2[mask_window > 0])
        if np.isnan(coeff).any():
            continue
        corr[ind] = coeff[1, 0]

    # corr[np.isnan(corr)] = 0
    corr = np.clip(corr, 0, 1)
    corr = np.abs(corr - np.max(corr))
    nib.save(nib.Nifti1Image(corr * mask, affine), args.output_corr)
    nib.save(nib.Nifti1Image(mask_diff, affine), args.output_diff)


if __name__ == "__main__":
    main()
