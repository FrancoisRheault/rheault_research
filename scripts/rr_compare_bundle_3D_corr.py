#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
ads
"""

import argparse
import itertools
import os
import sys

from dipy.io.stateful_tractogram import StatefulTractogram, Space
from dipy.io.utils import get_reference_info
from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
from scipy.ndimage import map_coordinates
from scipy.ndimage.filters import gaussian_filter

from scilpy.io.utils import (add_reference_arg,
                             add_overwrite_arg,
                             assert_inputs_exist,
                             assert_outputs_exist,
                             assert_output_dirs_exist_and_empty)
from scilpy.tractanalysis.streamlines_metrics import compute_tract_counts_map


def _build_arg_parser():
    p = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                                description=__doc__)

    p.add_argument('in_bundles', nargs='+',
                   help='List of input bundles.')
    p.add_argument('out_map',
                   help='Output correlation map (.nii).')
    p.add_argument('--out_dir',
                   help='Directory for the colored bundles.')

    add_reference_arg(p)
    add_overwrite_arg(p)

    return p


def cube_correlation(density_list, binary_list, size=3):
    elem = np.arange(-(size//2), size//2 + 1).tolist()
    cube_ind = np.array(list(itertools.product(elem, elem, elem)))

    union = np.sum(binary_list, axis=0)
    corr_map = np.zeros(density_list[0].shape)
    indices = np.array(np.where(union)).T
    for i, ind in enumerate(indices):
        # if i % 1000 == 0:
            # print(i, '/', len(indices))
        ind = tuple(ind)

        cube_list = []
        for density in density_list:
            cube = map_coordinates(density, (cube_ind+ind).T, order=0)

            if np.count_nonzero(cube) > 1:
                cube_list.append(cube.ravel())
        cov_matrix = np.triu(np.corrcoef(cube_list, cube_list), k=1)
        corr_map[ind] = np.average(cov_matrix[cov_matrix > 0])

    return corr_map


def main():
    parser = _build_arg_parser()
    args = parser.parse_args()

    assert_inputs_exist(parser, args.in_bundles)
    assert_outputs_exist(parser, args, args.out_map)
    assert_output_dirs_exist_and_empty(parser, args, args.out_dir)

    sft_list = []
    for filename in args.in_bundles:
        sft = load_tractogram(filename, 'same')
        sft.to_vox()
        sft.to_corner()
        sft_list.append(sft)

        if len(sft_list):
            if not is_header_compatible(sft_list[0], sft_list[-1]):
                parser.error('ERROR HEADER')

    density_list = []
    binary_list = []
    for sft in sft_list:
        density = compute_tract_counts_map(sft.streamlines,
                                           sft.dimensions).astype(float)
        binary = np.zeros(sft.dimensions)
        binary[density > 0] = 1
        binary_list.append(binary)

        density = gaussian_filter(density, 1) * binary
        density[binary < 1] += np.random.normal(0.0, 1.0,
                                                binary[binary < 1].shape)
        density_list.append(density)

    corr_map = cube_correlation(density_list, binary_list)
    nib.save(nib.Nifti1Image(corr_map, sft_list[0].affine), args.out_map)

    if args.out_dir:
        for i, sft in enumerate(sft_list):
            color_data = map_coordinates(corr_map, sft.streamlines._data.T,
                                         order=1)
            print(args.in_bundles[i], np.average(color_data))
            sft.data_per_point['color'] = sft.streamlines
            cmap = plt.get_cmap('jet')
            sft.data_per_point['color']._data = cmap(
                color_data / np.max(color_data))[:, 0:3] * 255
            basename, _ = os.path.splitext(os.path.basename(args.in_bundles[i]))
            out_name = os.path.join(args.out_dir, basename+'_'+str(i)+'.trk')
            save_tractogram(sft, out_name)
    print()


if __name__ == "__main__":
    main()
