#!/usr/bin/env python

import argparse
import sys
import os
import numpy as np
import numpy.ma as ma
import math
from math import cos, sin, radians, degrees
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.signal import argrelextrema
from scipy.ndimage.filters import gaussian_filter1d


def pol_to_car(r, theta):
    """theta in degrees

    returns tuple; (float, float); (x,y)
    """
    x = r * cos(radians(theta))
    y = r * sin(radians(theta))
    return x, y


def generate_circle(vec=None):
    if vec is None:
        radius = np.ones((360,))
    else:
        radius = np.dot(generate_circle(), vec) ** 8

    angles = range(0, 360, 1)
    circle = np.zeros((360, 2))
    for i, angle in enumerate(angles):
        circle[i, :] = pol_to_car(radius[i], angle)

    return circle


CIRCLE = generate_circle()


def mask_circle(input_circle, vec, angle=45):
    angle = math.radians(angle)
    mask_indices = np.dot(CIRCLE, vec)

    mask_indices[mask_indices < cos(angle)] = 0
    mask_indices[mask_indices >= cos(angle)] = 1

    masked_circle = input_circle[np.where(mask_indices)]
    return np.where(mask_indices)[0], masked_circle


def get_angle_vectors(p1, p2):
    ang1 = np.arctan2(*p1[::-1])
    ang2 = np.arctan2(*p2[::-1])
    return np.rad2deg((ang1 - ang2) % (2 * np.pi))


def get_angle_vectors_simple(v1, v2):
    v1 = np.array(v1)
    v2 = np.array(v2)
    return degrees(math.acos(np.dot(v1, v2) / (np.sqrt(np.dot(v1, v1)) * np.sqrt(np.dot(v2, v2)))))


def get_probability(lobe, in_angle, aperture, random_pick=1000):
    vec_small = pol_to_car(1, in_angle)

    prob_circle_indices, prob_circle_masked = mask_circle(
        lobe, vec_small, angle=aperture)

    normalized_prob = np.linalg.norm(prob_circle_masked, axis=1)
    normalized_prob /= np.sum(normalized_prob)

    draw = np.random.choice(len(normalized_prob), random_pick,
                            replace=True, p=normalized_prob)

    draw_indices, draw_counts = np.unique(draw, return_counts=True)
    draw_counts_circle = np.zeros((len(lobe)))
    draw_counts_circle[prob_circle_indices[draw_indices.T].T] = draw_counts
    return prob_circle_indices[draw_indices.T], draw_counts_circle


def segment_lobe(in_lobes, main_peaks):
    label = np.zeros((len(in_lobes), 4))
    for i, peak in enumerate(main_peaks):
        label[:, i] = np.clip(np.dot(in_lobes, np.array(peak)), 0, 1)
        label[:, i+2] = np.clip(-np.dot(in_lobes, np.array(peak)), 0, 1)
    label = np.argmax(label, axis=1)
    dict_lobe = {(0, 1): 1, (0, -1): 3, (1, 0): 0, (-1, 0): 2}
    return label, dict_lobe


def draw_lobe(in_lobe, prob_coordinates, draw_counts, in_angle, aperture,
              filename=None, ratio=None):
    LOBE_LABEL, LOBE_DICT = segment_lobe(in_lobe, ([1, 0], [0, 1]))
    vec_small = pol_to_car(1, in_angle)
    vec_large = pol_to_car(2, in_angle)
    ax = plt.axes()
    ax.scatter(in_lobe[:, 0], in_lobe[:, 1],
               marker='.', alpha=0.1)

    prob_coordinates_del = np.delete(
        prob_coordinates, np.where(draw_counts < 1), axis=0)
    draw_counts_del = np.delete(draw_counts, np.where(draw_counts < 1))

    ax.scatter(prob_coordinates_del[:, 0], prob_coordinates_del[:, 1],
               marker='.', c=draw_counts_del)
    ax.arrow(-vec_large[0], -vec_large[1], vec_small[0], vec_small[1],
             head_width=0.05, head_length=0.05, fc='k', ec='k')

    angle_between = get_angle_vectors(vec_small, [1, 0])
    low_x, low_y = pol_to_car(0.2, angle_between-aperture/2.0)
    high_x, high_y = pol_to_car(0.2, angle_between+aperture/2.0)
    start_x = -vec_large[0] + vec_small[0]
    start_y = -vec_large[1] + vec_small[1]

    ax.arrow(start_x, start_y, low_x, low_y,
             head_width=0.1, head_length=0.1, fc='k', ec='k')
    ax.arrow(start_x, start_y, high_x, high_y,
             head_width=0.1, head_length=0.1, fc='k', ec='k')
    ax.set_xlim([-1.5, 1.5])
    ax.set_ylim([-1.5, 1.5])
    ax.set_aspect('equal', 'datalim')

    closest_peak, closest_angle = 1000, 1000
    for key in LOBE_DICT.keys():
        tmp_angle = get_angle_vectors_simple(vec_small, list(key))
        if tmp_angle < closest_angle:
            closest_angle = tmp_angle
            closest_peak = tuple(key)

    tmp_ratio = sum(
        draw_counts[LOBE_LABEL == LOBE_DICT[closest_peak]]) / float(sum(draw_counts))
    if ratio is None:
        ratio = {(start_x, start_y): tmp_ratio}
    else:
        ratio[(start_x, start_y)] = tmp_ratio
    for tuple_key in ratio.keys():
        ax.scatter(tuple_key[0], tuple_key[1], cmap='inferno',
                   marker='o', c=ratio[tuple_key], vmin=0, vmax=1)
    if filename is None:
        plt.show()
    else:
        plt.savefig(filename, dpi=200)
    plt.close()

    return ratio


DESCRIPTION = 'ISMRM ABSTRACT 2019'


def buildArgsParser():
    p = argparse.ArgumentParser(description=DESCRIPTION,
                                formatter_class=argparse.RawTextHelpFormatter)

    p.add_argument('--step', default=5, type=int,
                   help='')
    p.add_argument('--aperture', default=40, type=int,
                   help='')
    p.add_argument('--mult_factor', default=0.2, type=float,
                   help='')
    p.add_argument('--nbr_iter', default=0, type=int,
                   help='')
    p.add_argument('--out_dir', default='tmp/',
                   help='')

    return p


def main():
    parser = buildArgsParser()
    args = parser.parse_args()

    if not os.path.isdir(args.out_dir):
        os.mkdir(args.out_dir)

    # circle_angle = range(0, 360, 1)
    peaks_1 = [1, 0]
    peaks_2 = [0, 1]
    xy_lobe = generate_circle(
        peaks_1) + generate_circle(peaks_2) * args.mult_factor
    xy_lobe /= np.max(xy_lobe)

    # ratio_dict_first = None
    ratio_dict_second = None
    for in_angle in range(0, 361, args.step):
        print '\n', in_angle, '/ 360'
        # weigthed_draw = np.zeros((len(xy_lobe)))
        total_iter_draw = np.zeros((len(xy_lobe)))
        draw_indices_init, draw_counts_init = get_probability(
            xy_lobe, in_angle, args.aperture, random_pick=100000)

        total_iter_draw = draw_counts_init
        prob_init = draw_counts_init / draw_counts_init.max()
        for iteration in range(args.nbr_iter):
            for i in range(len(draw_indices_init)):
                _, draw_counts = get_probability(
                    xy_lobe, draw_indices_init[i], args.aperture, random_pick=10000)
                total_iter_draw += draw_counts * \
                    prob_init[draw_indices_init[i]]
            draw_indices_init = np.where(total_iter_draw > 0)[0]
            prob_init = total_iter_draw / total_iter_draw.max()

        filename = os.path.join(args.out_dir,
                                'in{0}_ap{1}_mult{2}_iter{3}.png'.format(
                                    in_angle, args.aperture, 
                                    args.mult_factor, args.nbr_iter))
        ratio_dict_second = draw_lobe(xy_lobe, xy_lobe, total_iter_draw, in_angle, args.aperture, ratio=ratio_dict_second,
                                      filename=filename)


if __name__ == "__main__":
    main()
