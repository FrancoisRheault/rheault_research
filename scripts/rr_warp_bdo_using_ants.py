#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os

import numpy as np
import nibabel as nib

from dipy.tracking.streamline import transform_streamlines
from scipy import ndimage

from scilpy.io.utils import read_info_from_mb_bdo
from rheault_research.io.others import save_mb_bdo

DESCRIPTION = """
    Displace BDO file (from MI-Brain) using affine & warp from ANTs
    Only the center is moved, no modification of the radius
"""


def warp_point(point, deformation_data, affine):
    flip = [-1, -1, 1]
    inv_transfo = np.linalg.inv(affine)

    # To access the deformation information, we need to go in voxel space
    point_vox = transform_streamlines(point,
                                      inv_transfo)

    current_point_vox = np.array(point_vox).T
    current_point_vox_list = current_point_vox.tolist()

    x_def = ndimage.map_coordinates(deformation_data[..., 0],
                                    current_point_vox_list, order=1)
    y_def = ndimage.map_coordinates(deformation_data[..., 1],
                                    current_point_vox_list, order=1)
    z_def = ndimage.map_coordinates(deformation_data[..., 2],
                                    current_point_vox_list, order=1)

    # ITK is in LPS and nibabel is in RAS, a flip is necessary for ANTs
    final_point = np.array([flip[0]*x_def, flip[1]*y_def, flip[2]*z_def])

    # The deformation obtained is in worldSpace
    final_point += np.array(point).T
    return final_point


def buildArgsParser():
    p = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                                description=DESCRIPTION)

    p.add_argument('in_list', nargs='+',
                   help='Input list of BDO')

    p.add_argument('affine',
                   help='The Ants transform in a TXT format')

    p.add_argument('deformation',
                   help='Path of the file containing the \n'
                        'deformation field.')

    p.add_argument('append_name', default="_warp",
                   help='Append string to output. \n'
                        'Default: _warp')

    return p


def main():
    parser = buildArgsParser()
    args = parser.parse_args()
    transfo = np.loadtxt(args.affine)
    inv_transfo = np.linalg.inv(transfo)
    deformation = nib.load(args.deformation)
    deformation_data = np.squeeze(deformation.get_data())
    for fn in args.in_list:
        if not os.path.isfile(fn):
            parser.error('"{0}" must be a file!'.format(fn))

        _, _, center = read_info_from_mb_bdo(fn)

        lin_point = [center]
        lin_point = transform_streamlines(lin_point,
                                          inv_transfo)
        final_point = warp_point(lin_point, deformation_data, deformation.affine)
        root, ext = os.path.splitext(fn)
        save_mb_bdo(fn, final_point, root+args.append_name+ext)


if __name__ == "__main__":
    main()
