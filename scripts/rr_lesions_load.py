#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from collections import OrderedDict
import json

import nibabel as nib
import numpy as np
from dipy.io.streamline import load_tractogram
from scilpy.io.utils import (add_overwrite_arg,
                             assert_inputs_exist,
                             assert_outputs_exist)
from scilpy.tractanalysis.streamlines_metrics import compute_tract_counts_map

import scipy.ndimage as ndi

DESCRIPTION = """
    This script helps using QuickBundlesX, need a special branch.
    We cannot know the number of clusters in advance, also the tracking
    algorithm (DTI vs Det. vs Prob.) makes it even harder.
"""


def buildArgsParser():
    p = argparse.ArgumentParser(description=DESCRIPTION,
                                formatter_class=argparse.RawTextHelpFormatter)

    p.add_argument('lesions',
                   help='')
    p.add_argument('--bundle',
                   help='')
    p.add_argument('--map',
                   help='')
    p.add_argument('--output_json',  default='lesions_load.json',
                   help='')
    p.add_argument('--output_atlas',  default='lesions_atlas.nii.gz',
                   help='')

    add_overwrite_arg(p)

    return p


def compute_stats(map_data, lesions_atlas, voxel_size):
    lesions_load_dict = {}
    for i in np.unique(map_data)[1:]:
        section_dict = {}
        tmp_mask = np.zeros(map_data.shape)
        tmp_mask[map_data == i] = 1
        tmp_mask *= lesions_atlas

        lesions_sizes = []
        for j in np.unique(tmp_mask)[1:]:
            curr_size = np.count_nonzero(tmp_mask[tmp_mask == j])
            if curr_size >= 7:
                lesions_sizes.append(curr_size * voxel_size)
        if lesions_sizes:
            section_dict['total_volume'] = round(np.count_nonzero(tmp_mask), 3)
            section_dict['avg_volume'] = round(np.average(lesions_sizes), 3)
            section_dict['std_volume'] = round(np.std(lesions_sizes), 3)
            section_dict['all_volumes'] = lesions_sizes
            section_dict['lesions_count'] = len(lesions_sizes)
        lesions_load_dict[str(i).zfill(3)] = section_dict

    return lesions_load_dict


def main():
    parser = buildArgsParser()
    args = parser.parse_args()

    if (not args.bundle) and (not args.map):
        parser.error('One of the option --bundle or --map must be used')
    assert_inputs_exist(parser, [args.lesions],
                        optional=[args.bundle, args.map])
    assert_outputs_exist(parser, args, [args.output_atlas, args.output_json])

    lesions_img = nib.load(args.lesions)
    lesions_data = lesions_img.get_data().astype(np.int16)
    lesions_atlas, _ = ndi.label(lesions_data)

    if args.bundle:
        sft = load_tractogram(args.bundle, 'same')
        sft.to_vox()
        sft.to_corner()
        streamlines = sft.get_streamlines_copy()
        map_data = compute_tract_counts_map(streamlines,
                                            lesions_data.shape)
        map_data[map_data > 0] = 1
    else:
        map_img = nib.load(args.map)
        map_data = map_img.get_data().astype(np.uint8)

    voxel_size = np.prod(lesions_img.header.get_zooms()[0:3])
    lesions_load_dict = compute_stats(map_data, lesions_atlas, voxel_size)
    if len(np.unique(map_data)) > 2:
        map_data[map_data > 0] = 1
        total_lesions_load_dict = compute_stats(map_data, lesions_atlas,
                                                voxel_size)

        summarize_dict = {"sections": OrderedDict(lesions_load_dict)}
        summarize_dict["total"] = total_lesions_load_dict["01"]
    else:
        summarize_dict = {"total": lesions_load_dict.pop("01")}

    with open(args.output_json, 'w') as outfile:
        json.dump(summarize_dict, outfile)
    nib.save(nib.Nifti1Image(lesions_atlas, lesions_img.affine),
             args.output_atlas)


if __name__ == "__main__":
    main()
