#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os

from dipy.tracking.utils import length
import matplotlib.pyplot as plt
import numpy as np
import nibabel as nib
from scipy.ndimage import gaussian_filter

from scilpy.io.streamlines import load_trk_in_voxel_space
from scilpy.io.utils import (add_overwrite_arg,
                             assert_inputs_exist,
                             assert_outputs_exists)
from scilpy.tractanalysis import compute_robust_tract_counts_map


def _build_arg_parser():
    p = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
    p.add_argument('input',
                   help='Directory to the input trk in a format supported by Nibabel.')
    p.add_argument('prefix',
                   help='Prefix for output files.')
    add_overwrite_arg(p)
    return p


def main():
    parser = _build_arg_parser()
    args = parser.parse_args()
    print args.input
    dir_list = os.listdir(args.input)
    trk_list = []
    for i in dir_list:
        if os.path.splitext(i)[1] == '.trk':
            trk_list.append(os.path.join(args.input, i))

    output_map = args.prefix+'.nii.gz'
    output_txt = args.prefix+'.txt'
    output_fig = args.prefix+'.png'
    assert_outputs_exists(parser, args, [output_map, output_txt, output_fig])

    # Create output array
    dimensions = nib.streamlines.load(
        trk_list[0], lazy_load=True).header["dimensions"]
    affine = nib.streamlines.load(
        trk_list[0], lazy_load=True).header["voxel_to_rasmm"]

    stats_array = np.zeros((len(trk_list), 3))
    out_data = np.zeros(dimensions, dtype=np.float32)
    count = 0
    for i, input_trk_filename in enumerate(trk_list):
        vox_streamlines = load_trk_in_voxel_space(input_trk_filename)

        density = compute_robust_tract_counts_map(
            vox_streamlines, dimensions).astype(np.float32)
        density[density > 0] = 1.0

        streamline_count = len(vox_streamlines)
        volume = np.count_nonzero(density)
        length_avg = float(np.average(list(length(vox_streamlines))))

        stats_array[i] = [volume, streamline_count, length_avg]
        if volume > 40 and streamline_count > 20 and length_avg > 40:
            out_data += density
            count += 1

    vol_n_bins = [25, 50, 100, 250, 500, 1000, 2500,
                  5000, 10000, 25000, 50000, 100000, 250000]
    vol_bin_iter = 1
    while stats_array[:, 0].max() > vol_n_bins[vol_bin_iter]:
        vol_bin_iter +=1
        if vol_bin_iter == len(vol_n_bins):
            break

    count_n_bins = [25, 50, 100, 250, 500, 1000,
                    2500, 5000, 10000, 25000, 50000, 100000]
    count_bin_iter = 1
    while stats_array[:, 1].max() > count_n_bins[count_bin_iter]:
        count_bin_iter +=1
        if count_bin_iter == len(count_n_bins):
            break
        
    length_n_bins = 20

    _, axs = plt.subplots(3, 1, tight_layout=True)
    axs[0].hist(stats_array[:, 0], bins=vol_n_bins[0:vol_bin_iter+1])
    axs[0].set_title("volume")
    axs[1].hist(stats_array[:, 1], bins=count_n_bins[0:count_bin_iter+1])
    axs[1].set_title("count")
    axs[2].hist(stats_array[:, 2], bins=length_n_bins)
    axs[2].set_title("length")
    plt.xlim(0)
    plt.savefig(output_fig, dpi=300)

    out_file = open(output_txt, "w+")
    out_file.write("{0} {1}".format(count, len(trk_list)))
    nib.save(nib.Nifti1Image(out_data, affine), output_map)


if __name__ == "__main__":
    main()
