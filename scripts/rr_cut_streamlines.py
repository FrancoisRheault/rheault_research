#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import logging
import os

from dipy.io.stateful_tractogram import Origin, Space, StatefulTractogram
from dipy.io.streamline import save_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.io.streamlines import load_tractogram_with_reference
from scilpy.io.utils import (add_overwrite_arg, add_verbose_arg,
                             assert_inputs_exist, assert_outputs_exist)
from scilpy.tractanalysis.reproducibility_measures import get_endpoints_density_map
from scilpy.tractanalysis.tools import compute_streamline_segment
from scilpy.tractanalysis.uncompress import uncompress
from sklearn.cluster import KMeans


def split_heads_tails_kmeans(data):
    X = np.argwhere(data)
    k_means = KMeans(n_clusters=2).fit(X)
    mask_1 = np.zeros(data.shape)
    mask_2 = np.zeros(data.shape)

    mask_1[tuple(X[np.where(k_means.labels_ == 0)].T)] = 1
    mask_2[tuple(X[np.where(k_means.labels_ == 1)].T)] = 1

    return mask_1, mask_2


def intersects_two_rois(roi_data_1, roi_data_2, voxel_map):
    entry_found = False
    exit_found = False
    went_out_of_exit = False
    exit_roi_data = None
    in_strl_idx = None
    out_strl_idx = None

    # TODO simplify
    strl_indices = voxel_map

    logging.debug(strl_indices)
    for idx, point in enumerate(strl_indices):
        # logging.debug("Point: {}".format(point))
        if entry_found and exit_found:
            # Still add points that are in exit roi, to mimic entry ROI
            # This will need to be modified to correctly handle continuation
            if exit_roi_data[tuple(point)] > 0:
                if not went_out_of_exit:
                    out_strl_idx = idx
            else:
                went_out_of_exit = True
        elif entry_found and not exit_found:
            # If we reached the exit ROI
            if exit_roi_data[tuple(point)] > 0:
                exit_found = True
                out_strl_idx = idx
        elif not entry_found:
            # Check if we are in one of ROIs
            if roi_data_1[tuple(point)] > 0 or roi_data_2[tuple(point)] > 0:
                entry_found = True
                in_strl_idx = idx
                if roi_data_1[tuple(point)] > 0:
                    exit_roi_data = roi_data_2
                else:
                    exit_roi_data = roi_data_1

    return in_strl_idx, out_strl_idx


def cut_streamlines(streamlines, mask_img):
    density = get_endpoints_density_map(streamlines, mask_img.shape)
    density[density > 0] = 1
    mask_data = get_data_as_mask(mask_img)
    density[mask_data == 0] = 0

    roi_data_1, roi_data_2 = split_heads_tails_kmeans(density)

    final_streamlines = []
    (indices, points_to_idx) = uncompress(streamlines, return_mapping=True)

    for strl_idx, strl in enumerate(streamlines):
        logging.debug("Starting streamline")

        strl_indices = indices[strl_idx]
        logging.debug(strl_indices)

        in_strl_idx, out_strl_idx = intersects_two_rois(roi_data_1,
                                                        roi_data_2,
                                                        strl_indices)

        if in_strl_idx is not None and out_strl_idx is not None:
            points_to_indices = points_to_idx[strl_idx]
            logging.debug(points_to_indices)

            final_streamlines.append(
                compute_streamline_segment(strl, strl_indices, in_strl_idx,
                                           out_strl_idx, points_to_indices))

    return final_streamlines


def _build_arg_parser():
    p = argparse.ArgumentParser(
        description='Filters streamlines and only keeps the parts of '
                    'streamlines between the ROIs.')
    p.add_argument('in_tractogram',
                   help='Input tractogram file.')
    p.add_argument('in_mask',
                   help='Binary mask.')
    p.add_argument('out_tractogram',
                   help='Output tractogram file.')

    add_overwrite_arg(p)
    add_verbose_arg(p)
    return p


def main():
    parser = _build_arg_parser()
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    assert_inputs_exist(parser, [args.in_tractogram, args.in_mask])
    assert_outputs_exist(parser, args, args.out_tractogram)

    sft = load_tractogram_with_reference(parser, args, args.in_tractogram)
    sft.to_vox()
    sft.to_corner()

    mask_img = nib.load(args.in_mask)

    if not is_header_compatible(sft, mask_img):
        parser.error('Incompatible header.')

    new_streamlines = cut_streamlines(sft.streamlines, mask_img)

    if new_streamlines:
        new_sft = StatefulTractogram.from_sft(new_streamlines, sft,
                                              data_per_point={},
                                              data_per_streamline=sft.data_per_streamline)
        save_tractogram(new_sft, args.out_tractogram)
    else:
        logging.warn('No streamline intersected the masks. Not saving.')


if __name__ == "__main__":
    main()
