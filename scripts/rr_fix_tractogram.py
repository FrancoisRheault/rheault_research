#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import six

from dipy.tracking.streamline import transform_streamlines
import nibabel as nib
import numpy as np

from scilpy.io.utils import create_header_from_anat

def create_header_from_anat(reference):
    if isinstance(reference, six.string_types):
        reference = nib.load(reference)
    new_header = nib.streamlines.TrkFile.create_empty_header()

    new_header[nib.streamlines.Field.VOXEL_SIZES] = tuple(reference.header.
                                                          get_zooms())[:3]
    new_header[nib.streamlines.Field.DIMENSIONS] = tuple(reference.shape)[:3]
    new_header[nib.streamlines.Field.VOXEL_TO_RASMM] = (reference.header.
                                                        get_best_affine())
    affine = new_header[nib.streamlines.Field.VOXEL_TO_RASMM]

    new_header[nib.streamlines.Field.VOXEL_ORDER] = ''.join(
        nib.aff2axcodes(affine))

    return new_header


def _build_arg_parser():
    p = argparse.ArgumentParser(
        description="Fix the header of .trk files generated from StarTrack", 
        formatter_class=argparse.RawTextHelpFormatter)
    p.add_argument(
        'trk_file',
        help='Path to the .trk file to correct.')
    p.add_argument(
        'reference_file',
        help='Reference anatomy where the tracks should live.')
    p.add_argument(
        'output_trk_file', help='Path for the output corrected file.')
    p.add_argument('--affine_first', action='store_true',
                       help='The affine must be applied first')
    p.add_argument('-f, --force', dest='force', action='store_true',
                   help='if set, will overwrite the output files if present.')
    return p


def main():
    parser = _build_arg_parser()
    args = parser.parse_args()

    if not os.path.isfile(args.trk_file):
        parser.error("Input file doesn't exist.")

    if not os.path.isfile(args.reference_file):
        parser.error("Reference file doesn't exist")

    if os.path.isfile(args.output_trk_file) and not args.force:
        parser.error("Output file already exists. Use -f to overwrite.")
    
    t = nib.streamlines.load(args.trk_file)
    img = nib.load(args.reference_file)
    zooms = list(img.header.get_zooms())

    # Based on the test data.
    if not args.affine_first:
        aff = np.eye(4)
        aff[0:3, 3] = np.array(zooms[0:3]) / 2.0

        # Bring back to grid
        new_str = transform_streamlines(t.streamlines, aff)
        zooms.append(1)
        aff = np.eye(4) * np.array(zooms[0:3]+[1])
        aff[0:3, 3] = -0.5
        # aff[3,3] = 1
    else:
        new_str = transform_streamlines(t.streamlines, img.affine)

    new_tr = nib.streamlines.Tractogram(new_str, t.tractogram.data_per_streamline, 
                                       t.tractogram.data_per_point, 
                                       affine_to_rasmm=np.eye(4))

    hdr = create_header_from_anat(args.reference_file)

    nib.streamlines.save(new_tr, args.output_trk_file, header=hdr)


if __name__ == "__main__":
    main()
