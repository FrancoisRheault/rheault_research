#!/usr/bin/env python
import argparse
import os

import nibabel as nib
from nibabel.freesurfer.io import read_geometry
import numpy as np
import vtk


DESCRIPTION = """
    Conversion of a FreeSurfer surface to a *.vtk properly written either
    in world space OR in voxel space. Need the file avgraw.mgz (or .nii.gz)
    and the file orig.mgz (or .nii.gz) to visualize it in MI-Brain or SurfIce
    """


def buildArgsParser():

    p = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=DESCRIPTION)

    p.add_argument('input_surf',
                   help='Input surface, must be valid for Nibabel')

    p.add_argument('output',
                   help='Output filename (*.vtk)')

    p.add_argument('--xform',
                   help='Path of the copy-paste output from mri_info \n'
                        'Using: mri_info $input >> log.txt, \n'
                        'The file log.txt would be this parameter')

    p.add_argument('--to_lps', action='store_true',
                   help='Flip for Surfice/MI-Brain LPS')

    p.add_argument('-f', action='store_true', dest='force_overwrite',
                   help='force (overwrite output file if present)')

    return p


def extract_xform(filename):
    with open(filename) as f:
        content = f.readlines()
    names = [x.strip() for x in content]

    raw_xform = []
    for i in names:
        raw_xform.extend(i.split())

    start_read = 0
    for i, value in enumerate(raw_xform):
        if value == 'xform':
            start_read = int(i)
            break

    if start_read == 0:
        raise ValueError('No xform in that file...')

    matrix = np.eye(4)
    for i in range(3):
        for j in range(4):
            matrix[i, j] = float(raw_xform[13*i + (j*3) + 4+2+start_read][:-1])
    return matrix


def main():
    parser = buildArgsParser()
    args = parser.parse_args()

    if not os.path.isfile(args.input_surf):
        parser.error('"{0}" must be a file!'.format(args.input_surf))

    if args.xform and not os.path.isfile(args.xform):
        parser.error('"{0}" must be a file!'.format(args.xform))

    if os.path.isfile(args.output) and not args.force_overwrite:
        parser.error('"{0}" already exists! Use -f to overwrite it.'
                     .format(args.output))

    if os.path.splitext(args.input_surf)[1] != '.vtk':
        surface = read_geometry(args.input_surf)
        points = vtk.vtkPoints()
        triangles = vtk.vtkCellArray()

        flip_LPS = [-1, -1, 1]

        if args.xform:
            xform_matrix = extract_xform(args.xform)
            xform_translation = xform_matrix[0:3, 3]
        else:
            xform_translation = [0, 0, 0]

        # All possibles points in the VTK Polydata
        for vertex in surface[0]:
            id = points.InsertNextPoint((vertex[0:3]+xform_translation)*flip_LPS)

        # Each triangle is defined by an ID
        for vertex_id in surface[1]:
            triangle = vtk.vtkTriangle()
            triangle.GetPointIds().SetId(0, vertex_id[0]);
            triangle.GetPointIds().SetId(1, vertex_id[1]);
            triangle.GetPointIds().SetId(2, vertex_id[2]);
            triangles.InsertNextCell(triangle)

        polydata = vtk.vtkPolyData()
        polydata.SetPoints(points)
        polydata.SetPolys(triangles)
        polydata.Modified()

    else:
        reader1 = vtk.vtkPolyDataReader()
        reader1.SetFileName(args.input_surf)
        reader1.Update()
        polydata = reader1.GetOutput()

    if args.to_lps:
        flip_LPS = vtk.vtkMatrix4x4()
        flip_LPS.Identity()
        flip_LPS.SetElement(0, 0, -1)
        flip_LPS.SetElement(1, 1, -1)

        # Apply the transforms
        transform = vtk.vtkTransform()
        transform.Concatenate(flip_LPS)

        # Transform the polydata
        transform_polydata = vtk.vtkTransformPolyDataFilter()
        transform_polydata.SetTransform(transform)
        transform_polydata.SetInputData(polydata)
        transform_polydata.Update()
        polydata = transform_polydata.GetOutput()

    # Write the output *.vtk
    writter = vtk.vtkPolyDataWriter()
    writter.SetFileName(args.output)
    writter.SetInputData(polydata)
    writter.Update()

if __name__ == "__main__":
    main()
