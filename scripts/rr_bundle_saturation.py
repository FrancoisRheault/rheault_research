#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import time
import argparse
from itertools import chain
import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
import os
import random
import sys
from operator import itemgetter
import json
import logging
import copy

from scilpy.tractanalysis.robust_streamlines_metrics import compute_robust_tract_counts_map
from dipy.tracking.streamline import select_random_set_of_streamlines, set_number_of_points
from dipy.io.stateful_tractogram import Space, StatefulTractogram
from dipy.io.streamline import save_tractogram
import numpy as np
from numpy import corrcoef

from scilpy.io.utils import (add_overwrite_arg, add_reference, add_verbose_arg,
                             assert_inputs_exist,
                             assert_outputs_exist,
                             load_tractogram_with_reference)

from scilpy.utils.streamlines import (perform_streamlines_operation,
                                      intersection)

from scipy.stats import entropy
DESCRIPTION = """
    SATA style bundle saturation, save a text file of the graph.
    """


def build_args_parser():

    p = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='Substract (remove) streamlines from a file.')

    p.add_argument('bundle',
                   help='The bundle to analyse')

    p.add_argument('output_prefix',
                   help='Prefix for plot and text file')

    p.add_argument('--whole_brain',
                   help='Original whole brain tractogram')

    p.add_argument('--stepping_size', type=int, default=1000,
                   help='Size of chunk [%(default)s]'
                   '(e.g 1000 for bundles, 100 000 for whole brain)')

    p.add_argument('--recount', type=int, default=10,
                   help='Number of samples for each step [%(default)s]')

    p.add_argument('--log_level', default='INFO',
                   choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'],
                   help='Log level of the logging class')

    add_reference(p)
    add_overwrite_arg(p)

    return p


def compute_dice_voxel(density_1, density_2):
    numerator = 2 * np.count_nonzero(density_1 * density_2)
    denominator = np.count_nonzero(density_1) + np.count_nonzero(density_2)
    if denominator > 0:
        dice = numerator / float(denominator)
    else:
        dice = np.nan

    return dice


def main():
    parser = build_args_parser()
    args = parser.parse_args()

    assert_inputs_exist(parser, [args.bundle],
                        [args.whole_brain, args.reference])
    output_data_filename = '{}_data.json'.format(args.output_prefix)
    output_plot_filename = '{}_plot.png'.format(args.output_prefix)
    assert_outputs_exist(parser, args, [output_data_filename,
                                        output_plot_filename])

    logging.basicConfig(level=args.log_level)

    b_sft = load_tractogram_with_reference(parser, args, args.bundle)
    resampled_streamlines = set_number_of_points(b_sft.streamlines, 30)
    b_sft = StatefulTractogram(resampled_streamlines, b_sft, Space.RASMM)
    b_sft.to_vox()
    b_sft.to_corner()

    if args.whole_brain:
        wb_sft = load_tractogram_with_reference(parser, args, args.whole_brain)
        resampled_streamlines = set_number_of_points(wb_sft.streamlines, 30)
        wb_sft = StatefulTractogram(resampled_streamlines, wb_sft, Space.RASMM)
        wb_sft.to_vox()
        wb_sft.to_corner()

        # Computing indices both way for tractogram subsampling
        logging.debug('Pre-computing intersection for indices')
        _, b_indices = perform_streamlines_operation(intersection,
                                                     [b_sft.streamlines,
                                                      wb_sft.streamlines],
                                                     precision=0)
        _, wb_indices = perform_streamlines_operation(intersection,
                                                      [wb_sft.streamlines,
                                                       b_sft.streamlines],
                                                      precision=0)

    volume_dimensions = b_sft.dimensions
    voxel_volume = np.product(b_sft.voxel_sizes)

    # If the whole brain has above 10M streamlines, let's consider that
    # the bundle are saturated-ish at that point
    logging.debug('Generating the full bundle density map')
    total_density = compute_robust_tract_counts_map(b_sft.streamlines,
                                                    volume_dimensions).flatten()
    total_volume = float(np.count_nonzero(total_density) * voxel_volume)

    if args.log_level == 'DEBUG':
        print('Total volume of {}: {}mm3'.format(args.bundle, total_volume))
        print()

    starting = args.stepping_size
    stepping = args.stepping_size
    if args.whole_brain:
        stopping = len(wb_sft.streamlines)
    else:
        stopping = len(b_sft.streamlines)

    nb_bin = int(np.ceil((stopping - starting) / float(stepping)))

    measures_data = {}
    for key in ['volume', 'picked', 'corr', 'dice', 'entropy', 'slope']:
        measures_data[key] = np.zeros((nb_bin, args.recount))

    flatten_length = np.product(volume_dimensions)
    last_density_array = np.zeros((flatten_length, args.recount),
                                  dtype=np.int32)
    curr_density_array = np.zeros((flatten_length, args.recount),
                                  dtype=np.int32)

    # The saturation curve is sample using an increment defined by the user
    for bin_id, selection in enumerate(range(starting, stopping, stepping)):
        it = 0
        step_volume = []
        # Recount is necessary in case the sampling is just lucky/bad
        while it < args.recount:
            my_randoms = list(np.random.choice(np.arange(stopping), selection,
                                               replace=False))
            if args.whole_brain:
                _, _, tmp = np.intersect1d(my_randoms,
                                           wb_indices,
                                           return_indices=True)
                if len(tmp) <= 1:
                    it += 1
                    continue
                streamlines = b_sft.streamlines[tmp]
            else:
                streamlines = b_sft.streamlines[my_randoms]

            curr_density_array[:, it] = compute_robust_tract_counts_map(
                streamlines,
                volume_dimensions).flatten()

            # Accumulating data for slope/entropy, first 2 bins are skipped
            if bin_id <= 1:
                it += 1
                continue

            measures_data['volume'][bin_id, it] = np.count_nonzero(
                curr_density_array[:, it]) * voxel_volume
            measures_data['picked'][bin_id, it] = len(streamlines)
            measures_data['corr'][bin_id, it] = corrcoef(
                curr_density_array[:, it],
                total_density)[1, 0]
            measures_data['dice'][bin_id, it] = compute_dice_voxel(
                curr_density_array[:, it],
                total_density)
            measures_data['entropy'][bin_id, it] = entropy(
                curr_density_array[:, it]+0.0000001,
                last_density_array[:, it]+0.0000001)

            last_bin = max(bin_id-5, 0)
            x_ticks = np.arange(bin_id - last_bin) * (stepping / stopping)
            tmp_volume_window = measures_data['volume'][last_bin:bin_id,
                                                        it] / total_volume
            measures_data['slope'][bin_id, it], _ = np.polyfit(x_ticks,
                                                               tmp_volume_window,
                                                               1)
            it += 1

        last_density_array = copy.copy(curr_density_array)

        if args.log_level == 'DEBUG' and bin_id > 1:
            print('{} / {}'.format(selection, stopping - starting))

            for key in ['volume', 'picked', 'corr', 'dice', 'entropy', 'slope']:
                avg = np.average(measures_data[key][bin_id, :])
                std = np.std(measures_data[key][bin_id, :])
                print('{}: {} +/- {}'.format(key, avg, std))
            print()

    # Conversion to list for json
    data = {}
    for key in ['volume', 'picked', 'corr', 'dice', 'entropy', 'slope']:
        data[key] = measures_data[key].tolist()

    with open(output_data_filename, 'w') as outfile:
        json.dump(data, outfile)

    # Plotting the current bundle for easy Q/C
    avg = np.average(measures_data['volume'], axis=1)
    std = np.std(measures_data['volume'], axis=1)

    ax = plt.plot(range(starting, stopping, stepping),
                  avg)
    ax = plt.fill_between(range(starting, stopping, stepping),
                          avg - std, avg + std, alpha=0.5)

    # Only for simplicity and plotting (remove 1 dimension)
    spacing = np.max(measures_data['volume']) / 50.0
    color = ['red', 'orange', 'pink', 'blue', 'green']
    measures_data['volume'] = np.average(measures_data['volume'], axis=1)
    for i, key in enumerate(['picked', 'corr', 'dice', 'entropy', 'slope']):
        measures_data[key] = np.average(measures_data[key], axis=1)

        for j in range(1, len(measures_data['volume']), 4):
            if key == 'picked':
                text = '{:.1f}'.format(measures_data[key][j])
            else:
                text = '{:.3f}'.format(measures_data[key][j])
            x = j * args.stepping_size
            y = measures_data['volume'][j] - (i + 3) * spacing
            plt.text(x, y, text,
                     color=color[i], fontsize=2)

    plt.savefig(output_plot_filename, dpi=1200)


if __name__ == "__main__":
    main()
