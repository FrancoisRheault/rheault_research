#!/usr/bin/env python

import sys
import os

import nibabel as nib
import numpy as np   
from scilpy.tractanalysis.reproducibility_measures import compute_dice_voxel, compute_bundle_adjacency_voxel
from dipy.align.imaffine import transform_centers_of_mass


def main():
    input_1 = nib.load(sys.argv[1])
    input_2 = nib.load(sys.argv[2])

    affine = input_1.affine
    data_1 = input_1.get_fdata()
    data_2 = input_2.get_fdata()

    bary_1 = np.average(np.argwhere(data_1 == 1), axis=0)
    bary_2 = np.average(np.argwhere(data_2 == 1), axis=0)

    c_of_mass = transform_centers_of_mass(data_1, affine, data_2, affine)
    transformed_data_2 = c_of_mass.transform(data_2, interp='nearest')
    dice_voxel, _ = compute_dice_voxel(data_1, transformed_data_2)
    ba_voxel = compute_bundle_adjacency_voxel(data_1, data_2)
    delta_translation = c_of_mass.affine[0:3,3]

    print(dice_voxel, np.linalg.norm(delta_translation), delta_translation)


if __name__ == "__main__":
    main()

