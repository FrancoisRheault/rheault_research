#!/usr/bin/env python
import nibabel as nib
import numpy as np
import sys

# Path of the file to enlarge

img = nib.load(sys.argv[1])
data = img.get_data()
size = 300

start_x = np.ceil(size/2.0 - data.shape[0]/2.0).astype(np.int)
stop_x = np.ceil(size/2.0 + data.shape[0]/2.0).astype(np.int)
start_y = np.ceil(size/2.0 - data.shape[1]/2.0).astype(np.int)
stop_y = np.ceil(size/2.0 + data.shape[1]/2.0).astype(np.int)
start_z = np.ceil(size/2.0 - data.shape[2]/2.0).astype(np.int)
stop_z = np.ceil(size/2.0 + data.shape[2]/2.0).astype(np.int)

size = 300
if data.ndim == 4:
    new_volume = np.zeros((size, size, size) +
                          (data.shape[-1],), dtype=data.dtype)
    new_volume[start_x:stop_x, start_y:stop_y, start_z:stop_z, :] = data
elif data.ndim == 3:
    new_volume = np.zeros((size, size, size), dtype=data.dtype)
    new_volume[start_x:stop_x, start_y:stop_y, start_z:stop_z] = data

img.affine[0,3] += (150-145/2)
img.affine[0,3] += (150-181/2)
img.affine[0,3] += (150-155/2)
nib.save(nib.Nifti1Image(new_volume, img.affine), sys.argv[2])
