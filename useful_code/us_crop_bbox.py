#!/usr/bin/env python

import sys
import os
from PIL import Image
import numpy as np

def main():
    im = Image.open(sys.argv[1])
    im2 = im.crop(im.getbbox())
    im2.save(sys.argv[2])

if __name__ == "__main__":
    main()

