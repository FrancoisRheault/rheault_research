#!/usr/bin/env python

import os
import dicom as pydicom

def get_dwell_time(dicom_file):
    try:
      ETL = dicom_file.GradientEchoTrainLength
    except: 
      return -1
        
    WFS_pixel = dicom_file[0x2001, 0x1022].value
    wfs_hz = 434.214
    ES = WFS_pixel / (wfs_hz * (ETL + 1))

    dwell_time = ES * (ETL - 1)
    return dwell_time

dict_protocole_name = {}
for root, dirs, files in os.walk("./", topdown=False):
   for name in files:
        try:
          dcm = pydicom.read_file(os.path.join(root, name))
        except:
          continue

        if not dcm[0x0018, 0x1030].value in dict_protocole_name:
            dict_protocole_name[dcm[0x0018, 0x1030].value] = get_dwell_time(dcm)

for key in dict_protocole_name:
    print key, dict_protocole_name[key]

