#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rheault_research.utils import pdf as pdf_creator

html_info = pdf_creator.parse_report('report.html')
METHODS = """RecobundlesX is a multi-atlas, multi-parameters version of
Recobundles [1]. It is optimized for whole brain coverage using 39 major
well-known white matter pathways. The atlas is a customized population average
from 20 UKBioBank [2] and 20 Human Connectome Project [3] datasets co-registered
to MNI152 space.

This atlas was made to cover as much spatial extend as possible and explore as
much shape variability as possible. Recobundles is then run 27 times with
bundle-specific paramters and the results of each execution are used in
a majority-vote approach. This is inspired from the state-of-the-art in medical
images segmentation (before machine-learning) [4,5].

Then, a shape-based pruning is applied to each bundle to remove inconsistent or
spurious streamlines (outliers) [6].
"""

REFERENCES = """ [1] Garyfallidis, Eleftherios, et al. "Recognition of white matter bundles using local and global
    streamline-based registration and clustering." NeuroImage 170 (2018): 283-295.
[2] Sudlow, Cathie, et al. "UK biobank: an open access resource for identifying the causes of a wide range of complex
    diseases of middle and old age." Plos med 12.3 (2015): e1001779.
[3] Van Essen, David C., et al. "The WU-Minn human connectome project: an overview." Neuroimage 80 (2013): 62-79.
[4] Iglesias, Juan Eugenio, and Mert R. Sabuncu. "Multi-atlas segmentation of biomedical images: a survey."
    Medical image analysis 24.1 (2015): 205-219.
[5] Pipitone, Jon, et al. "Multi-atlas segmentation of the whole hippocampus and subfields using multiple automatically
    generated templates." Neuroimage 101 (2014): 494-512.
[6] Côté, Marc-Alexandre, et al. "Cleaning up the mess: tractography outlier removal using hierarchical QuickBundles
    clustering." 23rd ISMRM annual meeting. Toronto, Canada. 2015.
"""

pdf = pdf_creator.PDF(orientation='L', unit='mm', format='A4')
pdf.add_page()
pdf.titles('RBx_flow_V1', width=285)
pdf.add_cell_left('Status:', html_info[0], size_y=5, width=285)
pdf.add_cell_left('Started on:', html_info[1], size_y=5, width=285)
pdf.add_cell_left('Completed on:', html_info[2], size_y=5, width=285)
pdf.add_cell_left('Command:', html_info[3], size_y=5, width=285)
pdf.add_cell_left('Duration:', html_info[4], size_y=5, width=285)

pdf.add_cell_left('Methods:', METHODS, size_y=5, width=285)

pdf.add_page()
pdf.titles('RBx_flow_V1', width=285)
pdf.add_cell_left('References:', REFERENCES, size_y=5, width=285)
pdf.add_image('Left hemisphere', 'left.png', size_x=275, size_y=100, pos_x=10)

pdf.add_page()
pdf.titles('RBx_flow_V1', width=285)
pdf.add_image('Right hemisphere', 'right.png', size_x=250, size_y=70, pos_x=10)
pdf.add_image('Commisures++', 'comm.png', size_x=165, size_y=70, pos_x=10)

pdf.output('report.pdf', 'F')
