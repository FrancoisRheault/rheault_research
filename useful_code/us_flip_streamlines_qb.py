#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os

import nibabel as nib
import numpy as np

from scilpy.io.utils import add_overwrite_arg


def _build_args_parser():
    p = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
    p.add_argument('input_file', nargs='+',
                   help='Path of the input diffusion volumes.')
    p.add_argument('--axis',  action='store', type=str, choices=['x', 'y', 'z'],
                   help='Will flip the streamlines in this axis.'
                        'SUGGESTION: Commissural = x, Association = y, Projection = z')
    p.add_argument('--suffixe', default='_flip',
                   help='Path of the input diffusion volumes.')

    add_overwrite_arg(p)

    return p


def main():
    parser = _build_args_parser()
    args = parser.parse_args()

    for filename in args.input_file:
        tractogram = nib.streamlines.load(filename)
        streamlines = list(tractogram.streamlines)
        for i in range(len(streamlines)):

            if args.axis == 'x':
                if streamlines[i][0][0] > streamlines[i][-1][0]:
                    streamlines[i] = streamlines[i][::-1]
            elif args.axis == 'y':
                if streamlines[i][0][1] > streamlines[i][-1][1]:
                    streamlines[i] = streamlines[i][::-1]
            else:
                if streamlines[i][0][2] > streamlines[i][-1][2]:
                    streamlines[i] = streamlines[i][::-1]

        new_tractogram = nib.streamlines.Tractogram(streamlines,
                                                    affine_to_rasmm=np.eye(4))
        format = nib.streamlines.detect_format(filename)
        file = format(new_tractogram, header=tractogram.header)

        base, ext = os.path.splitext(filename)
        nib.streamlines.save(file, base+args.suffixe+ext)


if __name__ == "__main__":
    main()
