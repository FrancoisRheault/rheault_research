#!/usr/bin/env bash
BLUE='\033[0;34m'
LRED='\033[1;31m'
NC='\033[0m'
ColorPrint () { echo -e ${BLUE}${1}${NC}; }
# This script was made to simplify Emmanuel's pipeline
missing=1
if [ ${#} -eq 4 ]; then
    missing=0
fi

if [ ${missing} == 1 ]; then
    echo -e "${LRED}Missing some arguments."
    echo -e "  First argument : Tractogram or bundle file (*.trk)"
    echo -e "  Second argument : Original reference file (*.nii.gz)"
    echo -e "  Third argument : Directory for Laurent's ROI"
    echo -e "  Fourth argument : Output directory"
    exit 1
fi

mkdir ${4}
input_file=$(readlink -e ${1})
output_dir=$(readlink -e ${4})
base_name=$(basename ${1} .trk)
intial_folder=${PWD}

if [ ! -f ${output_dir}/tmp_dir/output1Warp.nii.gz ]; then
    mkdir ${output_dir}/tmp_dir/
    antsRegistrationSyNQuick.sh -d 3 -f ${2} -m ${3} -n 8 -o ${output_dir}/tmp_dir/output
    for i in $(dirname ${3})/*.nii.gz;
        do antsApplyTransforms -d 3 -i ${i} -r ${2} \
            -o ${output_dir}/tmp_dir/$(basename ${i}) -n NearestNeighbor \
            -t ${output_dir}/tmp_dir/output1Warp.nii.gz \
                ${output_dir}/tmp_dir/output0GenericAffine.mat;
            scil_image_math.py convert ${output_dir}/tmp_dir/$(basename ${i}) ${output_dir}/tmp_dir/$(basename ${i}) --data_type uint8 -f
    done
fi

cd ${output_dir}/tmp_dir/
rm cmd.sh

for hemi in 'L' 'R'
    do echo 'Segmenting AF_'${hemi}
    echo scil_filter_tractogram.py ${input_file} ${output_dir}/AF_${hemi}.trk \
        --drawn_roi Frontal_${hemi}.nii.gz either_end include \
        --drawn_roi Temporal_Occipital_inf_${hemi}.nii.gz either_end include \
        --drawn_roi CC.nii.gz any exclude \
        --drawn_roi IC_all_Subcortical_${hemi}.nii.gz any exclude \
        --drawn_roi EC_full_${hemi}.nii.gz any exclude --display_counts -v -f >> cmd.sh
done

echo 'Segmenting CC'
echo scil_filter_tractogram.py ${input_file} ${output_dir}/CC.trk --drawn_roi CC.nii.gz any include \
    --drawn_roi PrCGWM_PoCGWM_L.nii.gz either_end include \
    --drawn_roi PrCGWM_PoCGWM_R.nii.gz either_end include \
    --drawn_roi IC_all_Subcortical_L.nii.gz any exclude \
    --drawn_roi IC_all_Subcortical_R.nii.gz any exclude --display_counts -v -f >> cmd.sh

for hemi in 'L' 'R'
    do echo 'Segmenting PT_'${hemi}
    echo scil_filter_tractogram.py ${input_file} ${output_dir}/PT_${hemi}.trk \
        --drawn_roi Hypersignal_IC_${hemi}.nii.gz any include \
        --drawn_roi MB_${hemi}.nii.gz any include \
        --drawn_roi MO_${hemi}.nii.gz any include \
        --drawn_roi PrCGWM_PoCGWM_${hemi}.nii.gz either_end include \
        --drawn_roi CC.nii.gz any exclude --display_counts -v -f >> cmd.sh
done

parallel -P 5 < cmd.sh

cd ${intial_folder}
# rm ${output_dir}/tmp_dir/*.trk
