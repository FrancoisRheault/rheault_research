#!/usr/bin/env python

from selenium import webdriver
import os
import sys

def save_png_from_html(in_file, out_dir):
    driver = webdriver.Chrome('chromedriver')
    driver.set_window_size(642, 520)
    driver.get('file://'+os.path.abspath(in_file))
    basename = os.path.basename(in_file)
    driver.save_screenshot(os.path.abspath(os.path.join(out_dir, basename.replace('html', 'png'))))
    driver.quit()

for i in sys.argv[1:len(sys.argv)-1]:
	save_png_from_html(i, sys.argv[-1])
