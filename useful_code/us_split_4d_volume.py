#!/usr/bin/env python

import nibabel as nib
import os
import sys
from scilpy.utils.filenames import split_name_with_nii

"""""
    Convert a 4D image into a bunch of 3D ones
"""""

img = nib.load(sys.argv[1])
data = img.get_data()
basename = os.path.basename(split_name_with_nii(sys.argv[1])[0])

if not os.path.isdir(sys.argv[2]):
    os.mkdir(sys.argv[2])

for i in range(img.shape[3]):
    nib.save(nib.Nifti1Image(data[:, :, :, i], img.affine),
             sys.argv[2]+basename+'_'+str(i)+'.nii.gz')
