#!/usr/bin/env python

import argparse
import json
import logging
import itertools
import os
import sys

from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.segment.streamlines import filter_grid_roi
from scilpy.utils.filenames import split_name_with_nii

from dipy.align.imaffine import AffineMap
from scilpy.tractanalysis.reproducibility_measures import (compute_bundle_adjacency_voxel,
                                                           compute_dice_voxel,
                                                           compute_correlation)
from scilpy.tractanalysis.streamlines_metrics import compute_tract_counts_map


def compute_metrics(density_1, sft_2):
    # density_1 = compute_tract_counts_map(sft_1.streamlines, sft_1.dimensions)
    density_2 = compute_tract_counts_map(sft_2.streamlines, sft_2.dimensions)
    dice, w_dice = compute_dice_voxel(density_1, density_2)
    ba = compute_bundle_adjacency_voxel(density_1, density_2)
    corr = compute_correlation(density_1, density_2)

    return dice, w_dice, ba, corr


all_rois = ['CENTRAL_CC', 'CENTRAL_CS_L',
            'IC_L', 'LOWER_AXIAL_LIM',
            'MB_L', 'MEDIAL_SAGITTAL_LIM',
            'MID_SAGITTAL_PLANE', 'MO_L',
            'MO_L_NOT', 'POST_C_L',
            'POST_C_R', 'PRE_C_L',
            'PRE_C_R', 'TEMPORAL_ENTRY',
            'TEMPORAL_STEM']

cc = [('CENTRAL_CC', False),
      ('LOWER_AXIAL_LIM', True),
      ('POST_C_L', True),
      ('PRE_C_L', True),
      ('POST_C_R', True),
      ('PRE_C_R', True)]

pyt_l = [('IC_L', False),
         ('MB_L', False),
         ('MO_L', False),
         ('MO_L_NOT', True),
         ('MID_SAGITTAL_PLANE', True),
         ('POST_C_L', True),
         ('PRE_C_L', True)]

af_l = [('CENTRAL_CS_L', False),
        ('MEDIAL_SAGITTAL_LIM', True),
        ('POST_C_L', False),
        ('PRE_C_L', False),
        ('TEMPORAL_ENTRY', False),
        ('TEMPORAL_STEM', True)]
bundles = {'AF_L': af_l, 'PYT_L': pyt_l, 'CC': cc}

all_pairing = [['A111', 'B218', 'C317', 'D418'],
               ['A127', 'B228', 'C320', 'D426'],
               ['A136', 'B237', 'C338', 'D436'],
               ['A149', 'B246', 'C344', 'D443'],
               ['A156', 'B252', 'C359', 'D450']]
ori_tractogram = [os.path.join(sys.argv[2], '193441/whole_brain.trk'),
                  os.path.join(sys.argv[2], '219231/whole_brain.trk'),
                  os.path.join(sys.argv[2], '286650/whole_brain.trk'),
                  os.path.join(sys.argv[2], '486759/whole_brain.trk'),
                  os.path.join(sys.argv[2], '615441/whole_brain.trk')]
rater = sys.argv[1]

for ind, all_folders in enumerate(all_pairing):
    rois_dict = {}
    sft = load_tractogram(ori_tractogram[ind], 'same')
    for i, folder in enumerate(all_folders):
        print(folder)
        roi_impact = {'AF_L': {}, 'PYT_L': {}, 'CC': {}}
        rois_selected_ids_dict = {}
        for base in all_rois:
            ids = np.load(os.path.join(rater, folder,
                                       '{}_ind.npy'.format(base)))
            rois_selected_ids_dict[base] = ids

        for bundle_name, rules in bundles.items():
            filename = '{}.trk'.format(bundle_name)
            bundle_sft = load_tractogram(os.path.join(rater, folder,
                                                      filename), 'same')
            bundle_sft.to_vox()
            bundle_sft.to_corner()
            bundle_density = compute_tract_counts_map(bundle_sft.streamlines,
                                                      bundle_sft.dimensions)
            for roi_1 in rules:
                ids = np.arange(len(sft))
                name_1 = roi_1[0]
                is_exclude_1 = roi_1[1]
                for roi_2 in rules:
                    if roi_2 == roi_1:
                        continue
                    name_2 = roi_2[0]
                    is_exclude_2 = roi_2[1]
                    if is_exclude_2:
                        ids = np.setdiff1d(ids,
                                           rois_selected_ids_dict[name_2])
                    else:
                        ids = np.intersect1d(ids,
                                             rois_selected_ids_dict[name_2])

                curr_sft = sft[ids]
                curr_sft.to_vox()
                curr_sft.to_corner()
                curr_density = compute_tract_counts_map(curr_sft.streamlines,
                                                        curr_sft.dimensions)

                if name_1 not in roi_impact[bundle_name]:
                    roi_impact[bundle_name][name_1] = {'bundle_adjacency_voxels': [],
                                                        'dice_voxels': [],
                                                        'w_dice_voxels': [],
                                                        'density_correlation': []}
                dice, w_dice, ba, corr = compute_metrics(bundle_density, curr_sft)
                roi_impact[bundle_name][name_1]['dice_voxels'].append(dice)
                roi_impact[bundle_name][name_1]['w_dice_voxels'].append(w_dice)
                roi_impact[bundle_name][name_1]['bundle_adjacency_voxels'].append(ba)
                roi_impact[bundle_name][name_1]['density_correlation'].append(corr)

        with open(os.path.join(rater, folder, '{}_{}_roi_removal.json'.format(rater, folder)), 'w') as outfile:
            json.dump(roi_impact, outfile, indent=2, sort_keys=True)
