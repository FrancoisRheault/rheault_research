#!/usr/bin/env python

import sys
import os
from skimage.segmentation import flood, flood_fill
import imageio
import numpy as np

def main():
    image = imageio.imread(sys.argv[1])
    shape = image.shape[0:2] + (4,)
    new_image = np.zeros(shape).astype(np.uint8)
    selem = np.zeros((3,3))
    selem[0:3, 1] = 1
    selem[1, 0:3] = 1

    tmp = np.average(image, axis=-1)
    if tmp[50,50] < 5:
        tmp[tmp < 5] = 0
    elif tmp[50,50] > 250:
        tmp[tmp > 250] = 255

    if sys.argv[3] == 'fill':
        selem = np.zeros((3,3))
        selem[0:3, 1] = 1
        selem[1, 0:3] = 1

        filled_image = flood_fill(tmp, (50, 50), 1000, selem=selem, tolerance=1)

        filled_image[filled_image != 1000] = 255
        filled_image[filled_image == 1000] = 0
    else:
        filled_image = np.zeros(shape[0:2])
        filled_image[tmp == tmp[50, 50]] = -1
        filled_image[tmp != tmp[50, 50]] = 255
        filled_image[filled_image == -1] = 0


    new_image[:,:,0:3] = image[:,:,0:3]
    filled_image = filled_image
    new_image[:,:,3] = filled_image
    imageio.imwrite(sys.argv[2], new_image)
if __name__ == "__main__":
    main()

