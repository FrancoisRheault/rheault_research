#!/usr/bin/env python

import itertools
import os
import sys

import numpy as np

if sys.argv[1] == 'bn':
	left_cortex = np.arange(1, 211, 2)
	right_cortex = np.arange(2, 211, 2)
	subcortical = np.arange(211, 250)
elif sys.argv[1] == 'fs':
	left_cortex = np.arange(1001, 1036)
	right_cortex = np.arange(2001, 2036)
	subcortical = [8,10,11,12,13,16,17,18,26,28,47,49,50,51,52,53,54,58,60]
elif sys.argv[1] == 'lobe':
	left_cortex = np.arange(1, 5)
	right_cortex = np.arange(8, 13)
	subcortical_L = [6,7,15]
	subcortical_R = [13,14,15]
in_dir_name = sys.argv[2]

associations = list(itertools.combinations(left_cortex, r=2)) + list(itertools.combinations(right_cortex, r=2))
commisures = list(itertools.product(left_cortex, right_cortex)) + list(itertools.product(left_cortex, subcortical_R)) + list(itertools.product(subcortical_L, right_cortex))
projections = list(itertools.product(subcortical_L, left_cortex)) + list(itertools.product(subcortical_R, right_cortex))

commisures_dir_name = in_dir_name+'_comm/'
os.mkdir(commisures_dir_name)
for i,j in commisures:
	filename = '{}_{}.nii.gz'.format(i,j)
	if not os.path.isfile(os.path.join(in_dir_name, filename)):
		filename = '{}_{}.nii.gz'.format(j,i)
	if not os.path.isfile(os.path.join(in_dir_name, filename)):
		continue
	os.symlink(os.path.abspath(os.path.join(in_dir_name, filename)), os.path.abspath(os.path.join(commisures_dir_name, filename)))

associations_dir_name = in_dir_name+'_asso/'
os.mkdir(associations_dir_name)
for i,j in associations:
	filename = '{}_{}.nii.gz'.format(i,j)
	if not os.path.isfile(os.path.join(in_dir_name, filename)):
		filename = '{}_{}.nii.gz'.format(j,i)
	if not os.path.isfile(os.path.join(in_dir_name, filename)):
		continue
	os.symlink(os.path.abspath(os.path.join(in_dir_name, filename)), os.path.abspath(os.path.join(associations_dir_name, filename)))

projections_dir_name = in_dir_name+'_proj/'
os.mkdir(projections_dir_name)
for i,j in projections:
	filename = '{}_{}.nii.gz'.format(i,j)
	if not os.path.isfile(os.path.join(in_dir_name, filename)):
		filename = '{}_{}.nii.gz'.format(j,i)
	if not os.path.isfile(os.path.join(in_dir_name, filename)):
		continue
	os.symlink(os.path.abspath(os.path.join(in_dir_name, filename)), os.path.abspath(os.path.join(projections_dir_name, filename)))


