#!/usr/bin/env python

import sys
import os
import itertools
import copy
import pprint
import json
import nibabel as nib
groups = {'association_dorsal_L_group': ['AF_L', 'SLF_1_L', 'SLF_2_L', 'SLF_3_L', 'CG_L'],
          'association_dorsal_R_group': ['AF_R', 'SLF_1_R', 'SLF_2_R', 'SLF_3_R', 'CG_R'],
          'association_ventral_L_group': ['IFOF_L', 'ILF_L', 'OR_L', 'UF_L'],
          'association_ventral_R_group': ['IFOF_R', 'ILF_R', 'OR_R', 'UF_R'],
          'commissural_group': ['CC_1', 'CC_2a', 'CC_2b', 'CC_3', 'CC_4', 'CC_5',
                                'CC_6', 'CC_7', 'FX', 'CA', 'CP'],
          'cerebellum_group': ['SCP_L', 'SCP_R', 'ICP_L', 'ICP_R', 'MCP'],
          'projection_L_group': ['CST_L', 'CR_L'],
          'projection_R_group': ['CST_R', 'CR_R']}
sub_bundles_count = {'AF_L': 15, 'AF_R': 15,
                     'CA': 3,
                     'CC_1': 8,
                     'CC_2a': 9,
                     'CC_2b': 7,
                     'CC_3': 15,
                     'CC_4': 14,
                     'CC_5': 13,
                     'CC_6': 14,
                     'CC_7': 13,
                     'CG_L': 14, 'CG_R': 14,
                     'CP': 3,
                     'CR_L': 15, 'CR_R': 15,
                     'CST_L': 12, 'CST_R': 12,
                     'FX': 4,
                     'ICP_L': 3, 'ICP_R': 3,
                     'IFOF_L': 16, 'IFOF_R': 16,
                     'ILF_L': 13, 'ILF_R': 13,
                     'MCP': 7,
                     'OR_L': 8, 'OR_R': 8,
                     'SCP_L': None, 'SCP_R': None,
                     'SLF_1_L': 7, 'SLF_1_R': 7,
                     'SLF_2_L': 15, 'SLF_2_R': 15,
                     'SLF_3_L': 16, 'SLF_3_R': 16,
                     'UF_L': 9, 'UF_R': 9}

primary_subgroups_params = {"clustering_thresholds": [12],
                     "pruning_thresholds": [14, 12],
                     "pruning_neighbors_cluster_threshold": 10,
                     "neighbors_reduction_threshold": 18,
                     "nb_points": 20,
                     "progressive": True,
                     "nb_executions": 2,
                     "min_nb_votes": 1}

secondary_subgroups_params = {"clustering_thresholds": [10, 8, 6],
                       "pruning_thresholds": [10, 8, 6],
                       "pruning_neighbors_cluster_threshold": 6,
                       "neighbors_reduction_threshold": 16,
                       "nb_points": 20,
                       "progressive": False,
                       "nb_executions": 8,
                       "min_nb_votes": 4}

tertiary_subgroups_params = {"clustering_thresholds": [8, 6, 4],
                      "pruning_thresholds": [8, 7, 6],
                      "pruning_neighbors_cluster_threshold": 6,
                      "neighbors_reduction_threshold": 16,
                      "nb_points": 20,
                      "progressive": False,
                      "nb_executions": 8,
                      "min_nb_votes": 2}

primary_subgroups = copy.copy(primary_subgroups_params)
primary_subgroups["subgroups"] = {}
primary_subgroups["groups"] = groups.keys()
list_dict = []
for group_key in groups.keys():
    secondary_subgroups = copy.copy(secondary_subgroups_params)
    secondary_subgroups["groups"] = groups[group_key]
    secondary_subgroups["subgroups"] = {}
    for bundle in groups[group_key]:
        count = sub_bundles_count[bundle]
        sub_bundles_list = []
        if count:
            for i in range(count):
                sub_bundles_list.append(bundle+'_'+str(i))
        else:

            continue

        tmp_dict = copy.copy(tertiary_subgroups_params)
        tmp_dict["groups"] = sub_bundles_list
        secondary_subgroups["subgroups"][bundle] = tmp_dict

    primary_subgroups["subgroups"][group_key] = secondary_subgroups
list_dict.append(primary_subgroups)
#pprint.pprint(primary_subgroups)

with open('config_single_atlas.json', 'w') as outfile:
    json.dump(list_dict, outfile)