#!/usr/bin/env python

import nibabel as nib
import numpy as np
import sys

"""""
    Takes a bunch of 3D images (same dimensions) and convert them to a 4D image
"""""

filename = sys.argv[1:len(sys.argv) - 1]
output = sys.argv[len(sys.argv) - 1]

img_ref = nib.load(sys.argv[1])
full_shape = (img_ref.shape[0], img_ref.shape[1],
              img_ref.shape[2], len(filename))

full_volume = np.zeros(full_shape)
for i, volume_name in enumerate(filename):
    img = nib.load(volume_name)
    full_volume[:, :, :, i] = img.get_data()

nib.save(nib.Nifti1Image(full_volume, img_ref.affine), output)
