#!/usr/bin/env python

import nibabel as nib
import numpy as np
import os
import sys

from scilpy.utils.filenames import split_name_with_nii

# 1: Filename of the original RGB
# Also, see ts_fuse_rgb

img = nib.load(sys.argv[1])
data = img.get_data()
data.shape
name = ['_r','_g','_b']
basename, ext = split_name_with_nii(sys.argv[1])
for i in range(3):
    nib.save(nib.Nifti1Image(data[...,i].astype(np.uint8), img.affine), basename+name[i]+ext)
