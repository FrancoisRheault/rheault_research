#!/usr/bin/env python

import sys
import os
from openpyxl import load_workbook

# 1: Path to the 'mockup_metadata_sheet.xlsx'
# 2: Name of the folder (same as expert)


def main():
    wb = load_workbook(sys.argv[1])
    ws = wb["metadata"]
    list_sub = sorted(os.listdir(sys.argv[2]))
    for i in range(6,36, 2):
        ws['b'+str(i)] = list_sub[(i - 6) / 2]
    wb.save(os.path.join(sys.argv[2],
                         os.path.basename(os.path.abspath(sys.argv[3]))+'.xlsx'))


if __name__ == "__main__":
    main()

