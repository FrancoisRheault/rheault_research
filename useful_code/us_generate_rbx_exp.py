#!/usr/bin/env python

import nibabel as nib
import numpy as np
import sys
import os
import itertools
import random
import copy
root_folder = "/home/frheault/scratch/submission_tractostorm"
exp_folder = "/home/frheault/scratch/tractostorm_rbx_exp"
out_folder = "/home/frheault/scratch/tractostorm_rbx_exp/results"
config_file = os.path.join(exp_folder, "superset.json")


subjects = ["193441", "219231", "286650", "486759", "615441"]

experts = ["experts/benedictis", "experts/caverzasi", "experts/chenot",
           "experts/corrivetti", "experts/glavin", "experts/hau", "experts/jordan",
           "experts/maffei", "experts/panesar", "experts/petit", "experts/pomiecko",
           "experts/sarubbo", "experts/zemmoura"]

non_experts = ["non_experts/barakovic", "non_experts/chamberland",
               "non_experts/daducci", "non_experts/girard", "non_experts/goyette",
               "non_experts/morency", "non_experts/pestilli", "non_experts/poulin",
               "non_experts/romascano", "non_experts/tax", "non_experts/theaud"]
atlas = experts + non_experts

triplicate = ["", "_flip", "_patched"]


tractogram_clustering = ["8", "9", "10"]
seeds = ["1534", "1608", "1763", "1774", "1840"]
minimal_vote = ["0.00", "0.25", "0.50", "0.75", "1.00"]
exp_style = list(itertools.product([1, 2, 4, 8, 16], [1, 2, 4, 8, 16]))
if os.path.isfile('cmd_list.txt'):
    os.remove('cmd_list.txt')
for atlas_picking_seed in [7, 13, 17, 21, 22, 23, 40, 42, 69, 80]:
    random.seed(atlas_picking_seed)
    for subj in subjects:
        tmp_subjects = copy.copy(subjects)
        tmp_subjects.remove(subj)
        atlas_triplicate = [a+b for a,
                            b in itertools.product(tmp_subjects, triplicate)]
        atlas_triplicate = list(itertools.product(atlas, atlas_triplicate))

        with open('cmd_list.txt', 'a') as f:
            for tuple_i in exp_style:
                nb_atlas = tuple_i[0]
                nb_params = tuple_i[1]

                # Make sure all 'atlas' have the trk file
                all_exists = False
                while not all_exists:
                    atlas_pick = random.sample(atlas_triplicate, nb_atlas)

                    all_exists = True
                    for i in atlas_pick:
                        file_1 = os.path.join(
                            root_folder, i[0], i[1], "pyt_l_fixed_space_warp.trk")
                        file_2 = os.path.join(
                            root_folder, i[0], i[1], "pyt_r_fixed_space_warp.trk")
                        if not os.path.isfile(file_1) and not os.path.isfile(file_2):
                            all_exists = False

                atlas_folder = [os.path.join(root_folder, i[0], i[1])
                                for i in atlas_pick]
                transfo_file = os.path.join(exp_folder, "identity.txt")
                input_tractogram = os.path.join(
                    root_folder, "original_hemi", subj, "hemi_fuse_warp.trk")

                for seed in seeds:
                    for vote in minimal_vote:
                        output_dir = os.path.join(
                            out_folder, "ind_s"+subj+"_a"+str(nb_atlas)+"_s"+str(atlas_picking_seed)+"_p"+str(nb_params)+"_s"+seed+"_v"+vote)
                        cmd = "rr_recognize_bundles.py {0} {1} {2} {3} --output {4} --log DEBUG --multi {5} --minimal_vote {6} --tractogram_clustering_thr {7} --seeds {8} --inverse --num 8".format(input_tractogram,
                                                                                                                                                                                            config_file,
                                                                                                                                                                                            " ".join(
                                                                                                                                                                                                atlas_folder),
                                                                                                                                                                                            transfo_file,
                                                                                                                                                                                            output_dir,
                                                                                                                                                                                            nb_params,
                                                                                                                                                                                            vote,
                                                                                                                                                                                            " ".join(
                                                                                                                                                                                                tractogram_clustering),
                                                                                                                                                                                            seed)
                        print >> f, cmd

    f.close()

exp_style = list(itertools.product([1, 2, 3, 4], [1, 2, 4, 8, 16]))
gs_folder = "/home/frheault/scratch/tractostorm_rbx_exp/gold_standard/"
for atlas_picking_seed in [13, 17, 23, 42, 69]:
    random.seed(atlas_picking_seed)
    for subj in subjects:
        tmp_subjects = copy.copy(subjects)
        tmp_subjects.remove(subj)

        with open('cmd_list.txt', 'a') as f:
            for tuple_i in exp_style:
                nb_atlas = tuple_i[0]
                nb_params = tuple_i[1]

                atlas_pick = random.sample(tmp_subjects, nb_atlas)

                atlas_folder = [os.path.join(gs_folder, i)
                                for i in atlas_pick]
                transfo_file = os.path.join(exp_folder, "identity.txt")
                input_tractogram = os.path.join(
                    root_folder, "original_hemi", subj, "hemi_fuse_warp.trk")

                for seed in seeds:
                    for vote in minimal_vote:
                        output_dir = os.path.join(
                            out_folder, "gs_s"+subj+"_a"+str(nb_atlas)+"_s"+str(atlas_picking_seed)+"_p"+str(nb_params)+"_s"+seed+"_v"+vote)
                        cmd = "rr_recognize_bundles.py {0} {1} {2} {3} --output {4} --log DEBUG --multi {5} --minimal_vote {6} --tractogram_clustering_thr {7} --seeds {8} --inverse --num 8".format(input_tractogram,
                                                                                                                                                                                            config_file,
                                                                                                                                                                                            " ".join(
                                                                                                                                                                                                atlas_folder),
                                                                                                                                                                                            transfo_file,
                                                                                                                                                                                            output_dir,
                                                                                                                                                                                            nb_params,
                                                                                                                                                                                            vote,
                                                                                                                                                                                            " ".join(
                                                                                                                                                                                                tractogram_clustering),
                                                                                                                                                                                            seed)
                        print >> f, cmd
                    
f.close()

exp_style = list(itertools.product([5], [1, 2, 4, 8, 16]))
cur_folder = "/home/frheault/scratch/tractostorm_rbx_exp/gold_standard/curated/"
for subj in subjects:
    with open('cmd_list.txt', 'a') as f:
        for tuple_i in exp_style:
            nb_atlas = tuple_i[0]
            nb_params = tuple_i[1]

            transfo_file = os.path.join(exp_folder, "identity.txt")
            input_tractogram = os.path.join(
                root_folder, "original_hemi", subj, "hemi_fuse_warp.trk")

            for seed in seeds:
                for vote in minimal_vote:
                    output_dir = os.path.join(
                        out_folder, "cur_s"+subj+"_a"+str(nb_atlas)+"_p"+str(nb_params)+"_s"+seed+"_v"+vote)
                    cmd = "rr_recognize_bundles.py {0} {1} {2} {3} --output {4} --log DEBUG --multi {5} --minimal_vote {6} --tractogram_clustering_thr {7} --seeds {8} --inverse --num 8".format(input_tractogram,
                                                                                                                                                                                         config_file,
                                                                                                                                                                                         cur_folder,
                                                                                                                                                                                         transfo_file,
                                                                                                                                                                                         output_dir,
                                                                                                                                                                                         nb_params,
                                                                                                                                                                                         vote,
                                                                                                                                                                                         " ".join(
                                                                                                                                                                                             tractogram_clustering),
                                                                                                                                                                                         seed)
                    print >> f, cmd
                    
f.close()