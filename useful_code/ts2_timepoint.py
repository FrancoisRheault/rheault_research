#!/usr/bin/env python

import time
import argparse
import json
import logging
import itertools
import os
import sys

from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.segment.streamlines import filter_grid_roi
from scilpy.utils.filenames import split_name_with_nii

from dipy.align.imaffine import AffineMap
from scilpy.tractanalysis.reproducibility_measures import (compute_bundle_adjacency_voxel,
                                                           compute_dice_voxel,
                                                           compute_correlation)
from scilpy.tractanalysis.streamlines_metrics import compute_tract_counts_map


def compute_metrics(sft_1, sft_2):
    sft_1.to_vox()
    sft_1.to_corner()
    sft_2.to_vox()
    sft_2.to_corner()
    density_1 = compute_tract_counts_map(sft_1.streamlines, sft_1.dimensions)
    density_2 = compute_tract_counts_map(sft_2.streamlines, sft_2.dimensions)
    dice, w_dice = compute_dice_voxel(density_1, density_2)
    ba = compute_bundle_adjacency_voxel(density_1, density_2)
    corr = compute_correlation(density_1, density_2)

    return dice, w_dice, ba, corr


all_pairing = [['A111', 'B218', 'C317', 'D418'],
               ['A127', 'B228', 'C320', 'D426'],
               ['A136', 'B237', 'C338', 'D436'],
               ['A149', 'B246', 'C344', 'D443'],
               ['A156', 'B252', 'C359', 'D450']]

bundles = ['AF_L', 'PYT_L', 'CC']
rater = sys.argv[1]

aggregate_dict = {}
for bundle in bundles:
    effect_dice = np.zeros((5,3))
    effect_w_dice = np.zeros((5,3))
    effect_ba = np.zeros((5,3))
    effect_corr = np.zeros((5,3))
    for i, pairing in enumerate(all_pairing):
        print(bundle, pairing)
        for j in range(3):
            sft_1 = load_tractogram(os.path.join(rater, pairing[j], '{}.trk'.format(bundle)),
                                    'same')
            sft_2 = load_tractogram(os.path.join(rater, pairing[j+1], '{}.trk'.format(bundle)),
                                    'same')
            dice, w_dice, ba, corr = compute_metrics(sft_1, sft_2)

            effect_dice[i,j] = dice
            effect_w_dice[i,j] = w_dice
            effect_ba[i,j] = ba
            effect_corr[i,j] = corr

    aggregate_dict[bundle] = {'dice_voxels': effect_dice.tolist(),
                                'w_dice_voxels': effect_w_dice.tolist(),
                                'bundle_adjacency_voxels': effect_ba.tolist(),
                                'density_correlation': effect_corr.tolist()}

with open(os.path.join(rater, 'timepoint.json'), 'w') as outfile:
    json.dump(aggregate_dict, outfile, indent=2, sort_keys=True)
