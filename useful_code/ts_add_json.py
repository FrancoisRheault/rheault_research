#!/usr/bin/env python

import sys
import os
import json

with open(sys.argv[2]) as json_file:  
    data_in = json.load(json_file)

if os.path.isfile(sys.argv[3]):
    with open(sys.argv[3]) as json_file:  
        data_agg = json.load(json_file)
else:
    data_agg = {}

data_agg[sys.argv[1]] = data_in
with open(sys.argv[3], 'w') as outfile:
    json.dump(data_agg, outfile)