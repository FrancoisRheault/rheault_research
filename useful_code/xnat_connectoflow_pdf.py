#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rheault_research.utils import pdf as pdf_creator

html_info = pdf_creator.parse_report('report.html')
METHODS = """Connectoflow [1] is Nextflow [2] pipeline to generate Connectomics [3,4] matrices from tractography data.
The key steps in this version of Connectoflow are:
- Decompose: This step performs the parcel-to-parcel decomposition of the tractogram. It includes streamline-cutting
    operations to ensure streamlines have terminations in the provided atlas. Moreover, connection-wise cleaning processes
    that remove loops, discard spurious streamlines and discard incoherent curvatures are used to remove as many false
    positives as possible [5].
- COMMT: To further decrease the number of invalid streamlines and assign a quantitative weight to each streamline,
    Convex Optimization Modeling for Micro-structure Informed Tractography (COMMIT) [6,7] is used. This not only allows the
    removal of aberrant or spurious streamlines, but it was shown to increase reproducibility of connectivity measures by
    being more robust to various tractography biases. 
- AFD: Apparent Fiber Density (AFD) [8,9] is subsequently computed connection-wise using streamline orientations
    (fixel), which can be computationally burdensome if done on every pairwise connection of the connectome a posteriori.
    This step will provide a AFD-weighted connectivity matrix.
"""

REFERENCES = """[1] Rheault, Francois, et al. "Connectoflow: A cutting-edge Nextflow pipeline for structural connectomics",
    ISMRM 2021 Proceedings, #710.
[2] Di Tommaso, Paolo, et al. "Nextflow enables reproducible computational workflows.", Nature biotechnology 35.4 (2017): 316-319.
[3] Sotiropoulos, Stamatios N., and Andrew Zalesky. "Building connectomes using diffusion MRI: why, how and but.",
    NMR in Biomedicine 32.4 (2019): e3752.
[4] Yeh, Chun-Hung, et al. "Mapping structural connectivity using diffusion MRI: challenges and opportunities.",
    Journal of Magnetic Resonance Imaging (2020).
[5] Zhang, Zhengwu, et al. "Mapping population-based structural connectomes.", NeuroImage 172 (2018): 130-145.
[6] Daducci, Alessandro, et al. "COMMIT: convex optimization modeling for microstructure informed tractography.",
    IEEE transactions on medical imaging 34.1 (2014): 246-257.
[7] Schiavi, Simona, et al. "A new method for accurate in vivo mapping of human brain connections using microstructural,
    and anatomical information." Science advances 6.31 (2020): eaba8245.
[8] Raffelt, David A., et al. "Investigating white matter fibre density and morphology using fixel-based analysis.",
    Neuroimage 144 (2017): 58-73.
[9] Dhollander, Thijs, et al. "Fixel-based Analysis of Diffusion MRI: Methods, Applications, Challenges and
    Opportunities." (2020).
"""

pdf = pdf_creator.PDF(orientation='P', unit='mm', format='A4')
pdf.add_page()
pdf.titles('Connectoflow_V1')
pdf.add_cell_left('Status:', html_info[0], size_y=5)
pdf.add_cell_left('Started on:', html_info[1], size_y=5)
pdf.add_cell_left('Completed on:', html_info[2], size_y=5)
pdf.add_cell_left('Command:', html_info[3], size_y=5)
pdf.add_cell_left('Duration:', html_info[4], size_y=5)

pdf.add_cell_left('Methods:', METHODS, size_y=5)
pdf.add_cell_left('References:', REFERENCES, size_y=5)

pdf.add_page()
pdf.titles('Connectoflow_V1')
pdf.add_mosaic('Matrices',
               ['streamlines count weighted', 'volume weighted'],
               ['results_conn/me/Visualize_Connectivity/sc_matrix.png',
                'results_conn/me/Visualize_Connectivity/vol_matrix.png'],
               col=2, pos_x=20)
pdf.add_mosaic('',
               ['length weighted', 'FA weighted'],
               ['results_conn/me/Visualize_Connectivity/len_matrix.png',
                'results_conn/me/Visualize_Connectivity/fa_matrix.png'],
               col=2, pos_x=20)
pdf.add_mosaic('',
               ['COMMIT weighted', 'AFD weighted'],
               ['results_conn/me/Visualize_Connectivity/commit1_weights_matrix.png',
                'results_conn/me/Visualize_Connectivity/afd_fixel_matrix.png'],
               col=2, pos_x=20)
pdf.output('report.pdf', 'F')
