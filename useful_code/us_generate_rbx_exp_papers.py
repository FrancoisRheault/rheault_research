#!/usr/bin/env python

import nibabel as nib
import numpy as np
import sys
import os
import itertools
import random
import copy
root_folder = "/mnt/ext4_A/ongoing_projects/rbx_paper/original_data"
exp_folder = "/mnt/ext4_A/ongoing_projects/rbx_paper/atlases"
out_folder = "/mnt/ext4_A/ongoing_projects/rbx_paper/results"
config_file = os.path.join(exp_folder, "superset.json")

subjects = ["193441", "219231", "286650", "486759", "615441"]

atlas = ['alex_valcourt_caron','antoine_theberge','charles_poirier','gabrielle_grenier','guido_guberman',
          'john_begnoche','jon_haitz','kurt_schilling','leon_cai','maggie_roy','manon_edde',
          'marco_perez_caceres','mario_ocampo','noor_al_sharif','philippe_karan','pietro_bontempi',
          'sami_obaid','sara_bosticardo','simona_schiavi','viljami_sairanen']

triplicate = [['A111','B218','C317','D418'],
['A127','B228','C320','D426'],
['A136','B237','C338','D436'],
['A149','B246','C344','D443'],
['A156','B252','C359','D450']]


tractogram_clustering = ["10", "12", "15"]
seeds = ["1534", "1608", "1763", "1774", "1840"]
minimal_vote = ["0.25", "0.50", "0.75"]
exp_style = list(itertools.product([1, 2, 4, 8, 16], [1, 2, 4, 8, 16]))
if os.path.isfile('cmd_list.txt'):
    os.remove('cmd_list.txt')
for atlas_picking_seed in [7, 13, 17, 21, 22, 23, 40, 42, 69, 80]:
    random.seed(atlas_picking_seed)
    for subj in subjects:
        tmp_subjects = copy.copy(subjects)
        
        tmp_triplicates = []
        for i in range(len(triplicate)):
            if subjects.index(subj) != i:
                tmp_triplicates.extend(triplicate[i])

        atlas_triplicate = list(itertools.product(atlas, tmp_triplicates))

        with open('cmd_list.txt', 'a') as f:
            for tuple_i in exp_style:
                nb_atlas = tuple_i[0]
                nb_params = tuple_i[1]

                atlas_pick = random.sample(atlas_triplicate, nb_atlas)
                atlas_folder = [os.path.join(exp_folder, i[0], i[1])
                                for i in atlas_pick]
                transfo_file = os.path.join(exp_folder, "identity.txt")
                input_tractogram = os.path.join(
                    root_folder, subj, "whole_brain.trk")

                for seed in seeds:
                    for vote in minimal_vote:
                        output_dir = os.path.join(
                            out_folder, "ind_s"+subj+"_a"+str(nb_atlas)+"_s"+str(atlas_picking_seed)+"_p"+str(nb_params)+"_s"+seed+"_v"+vote)
                        cmd = "scil_recognize_multi_bundles.py {0} {1} {2} {3} --out_dir {4} --log_level DEBUG --multi {5} --minimal_vote {6} --tractogram_clustering_thr {7} --seeds {8} --inverse --processes 8".format(input_tractogram,
                                                                                                                                                                                            config_file,
                                                                                                                                                                                            " ".join(
                                                                                                                                                                                                atlas_folder),
                                                                                                                                                                                            transfo_file,
                                                                                                                                                                                            output_dir,
                                                                                                                                                                                            nb_params,
                                                                                                                                                                                            vote,
                                                                                                                                                                                            " ".join(
                                                                                                                                                                                                tractogram_clustering),
                                                                                                                                                                                          seed)
                        print(cmd, file=f)

    f.close()
