#!/usr/bin/env python

import sys
import numpy as np
import nibabel as nib

"""""
    Simply inverse a mask and save it using the same header
"""""

img = nib.load(sys.argv[1])
data = img.get_data().astype(np.uint16)
inv_data = np.ones(data.shape).astype(np.uint16)
inv_data[data == 1] = 0
nib.save(nib.Nifti1Image(data, img.affine), sys.argv[1])
nib.save(nib.Nifti1Image(inv_data, img.affine), sys.argv[2])
