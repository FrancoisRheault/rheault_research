#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import nibabel as nib
import numpy as np

from scilpy.utils.filenames import split_name_with_nii


"""""
    Semi-automatically removes duplicated B0, noise map at the end of a DWI
    and non desired bvals. Need to be adapted everytime...
"""""

in_dwi = sys.argv[1]
out_dwi = sys.argv[2]
in_encoding = sys.argv[3]
out_encoding = sys.argv[4]
to_remove = sys.argv[5:len(sys.argv)]

print to_remove
img = nib.load(in_dwi)
data = img.get_data()

with open(in_encoding) as f:
    content = f.readlines()
mrtrix_b = np.loadtxt(in_encoding)

new_data = np.delete(data, obj=np.array(to_remove), axis=3)
new_encoding = np.delete(mrtrix_b, obj=np.array(to_remove), axis=0)
np.savetxt(out_encoding, new_encoding, fmt="%.8f %.8f %.8f %f")
nib.save(nib.Nifti1Image(new_data, img.affine), out_dwi)

