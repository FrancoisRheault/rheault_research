#!/usr/bin/env python

import sys
import itertools
import nibabel as nib
from nibabel.affines import apply_affine
import numpy as np

from scilpy.io.utils import read_info_from_mb_bdo

def get_mask(size, center, transfo, dim):
    bottom_corner = center - size
    top_corner = center + size
    x_val = [bottom_corner[0], top_corner[0]]
    y_val = [bottom_corner[1], top_corner[1]]
    z_val = [bottom_corner[2], top_corner[2]]
    corner_world = list(itertools.product(x_val, y_val, z_val))
    corner_vox = apply_affine(np.linalg.inv(transfo), corner_world)

    # Since the filtering using a grid is so fast, we pre-filter
    # using a BB around the ellipsoid
    min_corner = np.min(corner_vox, axis=0) - 1.0
    max_corner = np.max(corner_vox, axis=0) + 1.5
    pre_mask = np.zeros(dim)
    min_x, max_x = int(max(min_corner[0], 0)), int(min(max_corner[0], dim[0]))
    min_y, max_y = int(max(min_corner[1], 0)), int(min(max_corner[1], dim[1]))
    min_z, max_z = int(max(min_corner[2], 0)), int(min(max_corner[2], dim[2]))

    pre_mask[min_x:max_x, min_y:max_y, min_z:max_z] = 1

    return pre_mask

def main():
    _, radius, center = read_info_from_mb_bdo(sys.argv[1])
    img = nib.load(sys.argv[2])
    affine = img.affine
    dim = img.shape
    data = img.get_fdata()
    mask = get_mask(radius, center, affine, dim)
    nib.save(nib.Nifti1Image(mask, affine), sys.argv[3])
    
    overlap_25 = []
    overlap_50 = []
    overlap_75 = []
    overlap_100 = []
    overlap_labels = data*mask
    for i in np.unique(overlap_labels)[1:]:
        ori_size = float(len(data[data == i]))
        new_size = len(overlap_labels[data == i])
        print(i, new_size/ori_size)

if __name__ == "__main__":
    main()

