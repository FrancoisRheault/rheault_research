#!/usr/bin/env python


import os
import sys

from dipy.io.streamline import load_tractogram
from dipy.io.utils import get_reference_info, is_header_compatible
import h5py
import nibabel as nib
import numpy as np

if os.path.isfile(sys.argv[3]):
    os.remove(sys.argv[3])

with h5py.File(sys.argv[3], 'w') as hdf5_file:
    ref_img = sys.argv[2]
    affine, dimensions, voxel_sizes, voxel_order = get_reference_info(ref_img)
    hdf5_file.attrs['affine'] = affine
    hdf5_file.attrs['dimensions'] = dimensions
    hdf5_file.attrs['voxel_sizes'] = voxel_sizes
    hdf5_file.attrs['voxel_order'] = voxel_order
    for basename in os.listdir(sys.argv[1]):
            filename = os.path.join(sys.argv[1], basename)
            sft = load_tractogram(filename, 'same')
            sft.to_vox()
            sft.to_corner()
            if not is_header_compatible(sft, ref_img):
                raise IOError('Wrong header!')
            base, _ = os.path.splitext(basename)
            keys = base.split('_')
            print(keys)
            group = hdf5_file.create_group('{}_{}'.format(keys[0], keys[1]))
            group.create_dataset('data', data=sft.streamlines._data,
                                 dtype=np.float16)
            group.create_dataset('offsets', data=sft.streamlines._offsets,
                                 dtype=np.int64)
            group.create_dataset('lengths', data=sft.streamlines._lengths,
                                 dtype=np.int32)
            for key in sft.data_per_streamline.keys():
                group.create_dataset(key, data=sft.data_per_streamline[key],
                                     dtype=np.float32)