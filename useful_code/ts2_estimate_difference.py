#!/usr/bin/env python

import argparse
import json
import logging
import os
import sys
import itertools

from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.segment.streamlines import filter_grid_roi
from scilpy.utils.filenames import split_name_with_nii
from scilpy.tractanalysis.reproducibility_measures import (compute_bundle_adjacency_voxel,
                                                           compute_dice_voxel)
from dipy.align.imaffine import (transform_centers_of_mass,
                                 AffineMap)

raters = sys.argv[1:len(sys.argv)-2]
subjects = ['193441','219231','286650','486759','615441']
# all_categories = [['A111', 'B218', 'C317', 'D418'],
#                   ['A127', 'B228', 'C320', 'D426'],
#                   ['A136', 'B237', 'C338', 'D436'],
#                   ['A149', 'B246', 'C344', 'D443'],
#                   ['A156', 'B252', 'C359', 'D450']]
all_categories = [['A111'], ['B218'], ['C317'], ['D418'],
                  ['A127'], ['B228'], ['C320'], ['D426'],
                  ['A136'], ['B237'], ['C338'], ['D436'],
                  ['A149'], ['B246'], ['C344'], ['D443'],
                  ['A156'], ['B252'], ['C359'], ['D450']]
all_pairing = []
for i in all_categories:
    all_pairing.append([])
    for j in itertools.product(raters, i):
        all_pairing[-1].append(os.path.join(*j))

filename = '{}.nii.gz'.format(sys.argv[-2])

rois_differences = {}
for i, all_folders in enumerate(all_pairing):
    # rois_differences[subjects[i]] = {}
    rois_differences[os.path.basename(all_folders[-1])] = {}
    curr_file_dice = []
    curr_file_ba = []
    curr_file_dist = []
    print(filename, all_folders)
    for folder_1, folder_2 in itertools.combinations(all_folders, r=2):
        img_1 = nib.load(os.path.join(folder_1, filename))
        img_2 = nib.load(os.path.join(folder_2, filename))
        if not is_header_compatible(img_1, img_2):
            raise ValueError('WRONG HEADER!')
        data_1 = get_data_as_mask(img_1)
        data_2 = get_data_as_mask(img_2)
        if np.count_nonzero(data_1) == 0 or np.count_nonzero(data_2) == 0:
            continue
        c_of_mass = transform_centers_of_mass(data_1, img_2.affine,
                                                data_2, img_2.affine)
        data_2_t = np.round(c_of_mass.transform(data_2))
        curr_file_dice.append(compute_dice_voxel(data_1, data_2_t)[0])
        curr_file_ba.append(compute_bundle_adjacency_voxel(data_1, data_2_t,
                                                            non_overlap=True))
        curr_file_dist.append(np.linalg.norm(c_of_mass.affine[0:3, 3]))

        tmp_dict = {'dice_coefficient': curr_file_dice,
                    'shape_adjacency': curr_file_ba,
                    'translation_distance': curr_file_dist}
        # rois_differences[subjects[i]] = tmp_dict
        rois_differences[os.path.basename(all_folders[-1])] = tmp_dict

with open(sys.argv[-1], 'w') as outfile:
    json.dump({split_name_with_nii(filename)[0]: rois_differences}, outfile, indent=2, sort_keys=True)
