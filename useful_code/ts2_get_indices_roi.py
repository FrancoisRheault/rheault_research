#!/usr/bin/env python

import argparse
import json
import logging
import os
import sys
import itertools

from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.segment.streamlines import filter_grid_roi
from scilpy.utils.filenames import split_name_with_nii
from scilpy.tractanalysis.reproducibility_measures import (compute_bundle_adjacency_voxel,
                                                           compute_dice_voxel)
from dipy.align.imaffine import (transform_centers_of_mass,
                                 AffineMap)

rater = sys.argv[1]
all_pairing = [['A111', 'B218', 'C317', 'D418'],
               ['A127', 'B228', 'C320', 'D426'],
               ['A136', 'B237', 'C338', 'D436'],
               ['A149', 'B246', 'C344', 'D443'],
               ['A156', 'B252', 'C359', 'D450']]
all_files = ['CENTRAL_CC.nii.gz', 'CENTRAL_CS_L.nii.gz',
             'IC_L.nii.gz', 'LOWER_AXIAL_LIM.nii.gz',
             'MB_L.nii.gz', 'MEDIAL_SAGITTAL_LIM.nii.gz',
             'MID_SAGITTAL_PLANE.nii.gz', 'MO_L.nii.gz',
             'MO_L_NOT.nii.gz', 'POST_C_L.nii.gz',
             'POST_C_R.nii.gz', 'PRE_C_L.nii.gz',
             'PRE_C_R.nii.gz', 'TEMPORAL_ENTRY.nii.gz',
             'TEMPORAL_STEM.nii.gz']
ori_tractogram = [os.path.join(sys.argv[2], '193441/whole_brain.trk'),
                  os.path.join(sys.argv[2], '219231/whole_brain.trk'),
                  os.path.join(sys.argv[2], '286650/whole_brain.trk'),
                  os.path.join(sys.argv[2], '486759/whole_brain.trk'),
                  os.path.join(sys.argv[2], '615441/whole_brain.trk')]

for ind, all_folders in enumerate(all_pairing):
    sft = load_tractogram(ori_tractogram[ind], 'same')
    for filename in all_files:
        for folder in all_folders:
            print(os.path.join(rater, folder, filename))
            new_ind_filename = filename.replace('.nii.gz', '_ind.npy')
            if os.path.isfile(new_ind_filename):
                continue
            img_1 = nib.load(os.path.join(rater, folder, filename))
            _, indices = filter_grid_roi(sft, get_data_as_mask(img_1),
                                         'any', False)
            indices_not = np.setdiff1d(range(len(sft)), indices)
            new_ind_filename = filename.replace('.nii.gz', '_ind.npy')
            # new_ind_not_filename = filename.replace('.nii.gz', '_ind_NOT.npy')
            np.save(os.path.join(rater, folder, new_ind_filename),
                    indices.astype(np.uint32))
            # np.save(os.path.join(rater, folder, new_ind_not_filename),
            #         indices_not.astype(np.uint16))
