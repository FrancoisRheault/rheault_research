#!/usr/bin/env python

import imageio
import sys

"""""
    Using a list of jpeg or png, create a smooth video
    #1 to #(n-1) : List of images
    #n : Output filename
"""""

filename = sys.argv[1:len(sys.argv) - 1]
output = sys.argv[len(sys.argv) - 1]
writer = imageio.get_writer(output, fps=36)
for i in filename:
    writer.append_data(imageio.imread(i))
    print i
writer.close()
