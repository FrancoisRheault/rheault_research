#!/usr/bin/env python

import sys
import os
import json 

input_list = sys.argv[1:len(sys.argv) - 1]
out_name = sys.argv[-1]
if os.path.isfile(out_name):
    data_agg = {}
else:
    data_agg = {}

for input_name in input_list:
    with open(input_name) as json_file:  
        data_in = json.load(json_file)
    print input_name, data_in.keys()
    for key in data_in.keys():
        if key in data_agg:
            data_agg[key].append(data_in[key])
        else:
            data_agg[key] = [data_in[key]]

with open(out_name, 'w') as outfile:
    json.dump(data_agg, outfile)
