#!/usr/bin/env python

import vtk
import sys

"""""
    Takes 2 VTK polydata and fuses them into one, could be easily improved
    to a list of X files.
"""""

reader1 = vtk.vtkPolyDataReader()
reader1.SetFileName(sys.argv[1])
reader1.Update()
lh_poly = reader1.GetOutput()

reader2 = vtk.vtkPolyDataReader()
reader2.SetFileName(sys.argv[2])
reader2.Update()
rh_poly = reader2.GetOutput()

appendFilter = vtk.vtkAppendPolyData()
appendFilter.AddInputData(lh_poly)
appendFilter.AddInputData(rh_poly)
appendFilter.Update()

writter = vtk.vtkPolyDataWriter()
writter.SetFileName(sys.argv[3])
writter.SetInputData(appendFilter.GetOutput())
writter.Update()
