#! /usr/bin/env python

import nibabel as nib
import numpy as np
import sys

"""""
    Give a 4D image and the script add them (grid-wise), compute an average
    and save it. (Same dimensions, voxel-size, affine, etc.)
"""""

img = nib.load(sys.argv[1])
data = img.get_data()
data_avg = np.average(data, axis=3)
nib.save(nib.Nifti1Image(data_avg, img.affine), sys.argv[2])
