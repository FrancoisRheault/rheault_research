#!/usr/bin/env python

import sys
import os

import nibabel as nib
import numpy as np

def main():
    img_1 = nib.load(sys.argv[1])
    data_1 = img_1.get_fdata()
    data_2 = nib.load(sys.argv[2]).get_fdata()
    data_3 = nib.load(sys.argv[3]).get_fdata()

    new_data = np.zeros(data_1.shape + (3,))
    new_data[..., 0] = (data_1 / np.max(data_1)) * 255
    new_data[..., 1] = (data_2 / np.max(data_2)) * 255
    new_data[..., 2] = (data_3 / np.max(data_3)) * 255
    nib.save(nib.Nifti1Image(new_data.astype(np.uint8), img_1.affine), sys.argv[4])

    # max_real = np.max([np.max(data_1), np.max(data_2), np.max(data_3)])
    # new_data = np.zeros(data_1.shape + (3,))
    # new_data[..., 0] = (data_1 / max_real) * 255
    # new_data[..., 1] = (data_2 / max_real) * 255
    # new_data[..., 2] = (data_3 / max_real) * 255
    # nib.save(nib.Nifti1Image(new_data.astype(np.uint8), img_1.affine), sys.argv[4])

if __name__ == "__main__":
    main()

