#!/usr/bin/env python

import sys
import os
import numpy as np

matrix = np.loadtxt(sys.argv[1])
np.savetxt(sys.argv[2], np.linalg.inv(matrix))