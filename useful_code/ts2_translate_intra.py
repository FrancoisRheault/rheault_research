#!/usr/bin/env python

import time
import argparse
import json
import logging
import itertools
import os
import sys

from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.segment.streamlines import filter_grid_roi
from scilpy.utils.filenames import split_name_with_nii

from dipy.align.imaffine import AffineMap
from scilpy.tractanalysis.reproducibility_measures import (compute_bundle_adjacency_voxel,
                                                           compute_dice_voxel,
                                                           compute_correlation)
from scilpy.tractanalysis.streamlines_metrics import compute_tract_counts_map


all_rois = ['CENTRAL_CC', 'CENTRAL_CS_L',
            'IC_L', 'LOWER_AXIAL_LIM',
            'MB_L', 'MEDIAL_SAGITTAL_LIM',
            'MID_SAGITTAL_PLANE', 'MO_L',
            'MO_L_NOT', 'POST_C_L',
            'POST_C_R', 'PRE_C_L',
            'PRE_C_R', 'TEMPORAL_ENTRY',
            'TEMPORAL_STEM']


def compute_metrics(density_1, sft_2):
    # density_1 = compute_tract_counts_map(sft_1.streamlines, sft_1.dimensions)
    density_2 = compute_tract_counts_map(sft_2.streamlines, sft_2.dimensions)
    dice, w_dice = compute_dice_voxel(density_1, density_2)
    ba = compute_bundle_adjacency_voxel(density_1, density_2)
    corr = compute_correlation(density_1, density_2)

    return dice, w_dice, ba, corr


cc = [('CENTRAL_CC', False),
      ('LOWER_AXIAL_LIM', True),
      ('POST_C_L', True),
      ('PRE_C_L', True),
      ('POST_C_R', True),
      ('PRE_C_R', True)]

pyt_l = [('IC_L', False),
         ('MB_L', False),
         ('MO_L', False),
         ('MO_L_NOT', True),
         ('MID_SAGITTAL_PLANE', True),
         ('POST_C_L', True),
         ('PRE_C_L', True)]

af_l = [('CENTRAL_CS_L', False),
        ('MEDIAL_SAGITTAL_LIM', True),
        ('POST_C_L', False),
        ('PRE_C_L', False),
        ('TEMPORAL_ENTRY', False),
        ('TEMPORAL_STEM', True)]
bundles = {'AF_L': af_l, 'PYT_L': pyt_l, 'CC': cc}

all_pairing = ['C317', 'C320', 'C338', 'C344', 'C359']
ori_tractogram = [os.path.join(sys.argv[2], '193441/whole_brain.trk'),
                  os.path.join(sys.argv[2], '219231/whole_brain.trk'),
                  os.path.join(sys.argv[2], '286650/whole_brain.trk'),
                  os.path.join(sys.argv[2], '486759/whole_brain.trk'),
                  os.path.join(sys.argv[2], '615441/whole_brain.trk')]

rater = sys.argv[1]
folder = all_pairing[int(sys.argv[3])]
sft = load_tractogram(ori_tractogram[int(sys.argv[3])], 'same')
sft.to_vox()
sft.to_corner()

rois_dict = {}
rois_dict_id = {}
for filename in all_rois:
    roi_img = nib.load(os.path.join(rater, folder,
                                    '{}.nii.gz'.format(filename)))
    if not is_header_compatible(sft, roi_img):
        raise ValueError('WRONG HEADER!')
    rois_dict[filename] = get_data_as_mask(roi_img)
    rois_dict_id[filename] = np.load(os.path.join(rater, folder,
                                                  '{}_ind.npy'.format(filename)))


shift = np.zeros((6, 3), dtype=np.int8)
shift[:, 0] = [-5, -3, -1, 1, 3, 5]
comb_roll = []
comb_roll.append(shift.tolist())
comb_roll.append(np.roll(shift, shift=1, axis=1).tolist())
comb_roll.append(np.roll(shift, shift=2, axis=1).tolist())

aggregate_dict = {'AF_L': {}, 'PYT_L': {}, 'CC': {}}
for bundle_name, rules in bundles.items():
    filename = '{}.trk'.format(bundle_name)
    baseline_sft = load_tractogram(os.path.join(rater, folder,
                                                filename), 'same')
    baseline_sft.to_vox()
    baseline_sft.to_corner()
    bundle_density = compute_tract_counts_map(baseline_sft.streamlines,
                                              baseline_sft.dimensions)
    for roi_1 in rules:
        name_1 = roi_1[0]
        is_exclude_1 = roi_1[1]
        effect_dice = np.zeros((3, 6))
        effect_w_dice = np.zeros((3, 6))
        effect_ba = np.zeros((3, 6))
        effect_corr = np.zeros((3, 6))
        for i, axis in enumerate(['x', 'y', 'z']):
            for j, curr_comb_roll in enumerate(comb_roll[i]):
                print(curr_comb_roll)
                data_roll = np.roll(rois_dict[name_1], shift=curr_comb_roll[0],
                                    axis=0)
                data_roll = np.roll(data_roll, shift=curr_comb_roll[1], axis=1)
                data_roll = np.roll(data_roll, shift=curr_comb_roll[2], axis=2)
                ids = np.arange(len(sft))
                for roi_2 in rules:
                    name_2 = roi_2[0]
                    is_exclude_2 = roi_2[1]
                    if name_2 == name_1:
                        continue
                    if is_exclude_2:
                        ids = np.setdiff1d(ids, rois_dict_id[name_2])
                    else:
                        ids = np.intersect1d(ids, rois_dict_id[name_2])

                new_sft, _ = filter_grid_roi(
                    sft[ids], data_roll, 'any', is_exclude_1)

                if len(ids):
                    dice, w_dice, ba, corr = compute_metrics(
                        bundle_density, new_sft)
                    effect_dice[i, j] = dice
                    effect_w_dice[i, j] = w_dice
                    effect_ba[i, j] = ba
                    effect_corr[i, j] = corr

        aggregate_dict[bundle_name][name_1] = {'dice_voxels': effect_dice.tolist(),
                                  'w_dice_voxels': effect_w_dice.tolist(),
                                  'bundle_adjacency_voxels': effect_ba.tolist(),
                                  'density_correlation': effect_corr.tolist()}

with open(os.path.join(rater, folder, '{}_{}_translate_impact.json'.format(rater, folder)), 'w') as outfile:
    json.dump(aggregate_dict, outfile, indent=2, sort_keys=True)
