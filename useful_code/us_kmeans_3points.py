#!/usr/bin/env python

import nibabel as nib
import numpy as np
from dipy.io.stateful_tractogram import StatefulTractogram, Space, Origin
from dipy.io.streamline import save_tractogram, load_tractogram
import matplotlib.pyplot as plt
from copy import deepcopy
from time import time
from sklearn.cluster import KMeans
from dipy.tracking.streamline import set_number_of_points
from dipy.segment.clustering import ClusterMap
import itertools
from scilpy.tractanalysis.features import get_streamlines_centroid
from operator import itemgetter
from dipy.tracking.streamlinespeed import length

timer = time()
# sft = load_tractogram('me_s0__ensemble.trk', 'same')
sft = load_tractogram('/home/frheault/Datasets/xnat/connectoflow/in_dir/ordered.trk', 'same')
print('loading:', time()-timer)


timer = time()
streamlines = set_number_of_points(sft.streamlines[0:100000], 3)
print('resample:', time()-timer)

timer = time()
X = streamlines._data
total_labels = 100
kmeans = KMeans(n_clusters=total_labels, random_state=0, copy_x=True, precompute_distances=True, n_jobs=1).fit(X)
print('kmeans at {}:'.format(total_labels), time()-timer)

timer = time()
streamlines = set_number_of_points(sft.streamlines, 3)
kmeans.labels_ = kmeans.predict(streamlines._data)
print('predict', time()-timer)

unique_labels = np.unique(kmeans.labels_)
# comb_list = list(itertools.combinations(unique_labels, r=3))
# comb_list.extend(zip(unique_labels, unique_labels, unique_labels))

timer = time()
cluster_map = ClusterMap()
my_dict = {}
for i in range(len(sft)):
    labels = np.sort([kmeans.labels_[3*i], kmeans.labels_[(3*i)+2]])
    labels = tuple([labels[0], kmeans.labels_[(3*i)+1], labels[-1]])
    if labels in my_dict:
        my_dict[labels].append(i)
    else:
        my_dict[labels] = [i]

indices_list = list(my_dict.values())
centroids_list = []
for indices in indices_list:
    streamlines = sft.streamlines[indices]
    centroids_list.append(get_streamlines_centroid(streamlines, 20))

cluster_map.refdata = sft.streamlines
cluster_map.centroids = centroids_list
cluster_map.nb_centroids = len(cluster_map.centroids)
cluster_map.indices = indices_list
print('construct ClusterMap', time()-timer)

timer = time()
comb_list = list(my_dict.keys())
for i in range(cluster_map.nb_centroids):
    print(length(cluster_map.centroids[i]))
    # print(len(sft))
    cluster_streamlines = sft.streamlines[cluster_map.indices[i]]

    if len(cluster_map.indices[i]) > 5:
        tmp_sft = StatefulTractogram.from_sft(cluster_streamlines, sft)
        save_tractogram(tmp_sft, 'lol100/{}_{}_{}.trk'.format(*comb_list[i]))
print('saving', time()-timer)

# timer = time()
# data = []
# for i in sft.streamlines._data:
#     data.append([i, i+[0.25,0.25,0.25]])
# new_sft = StatefulTractogram.from_sft(data, sft)
# new_sft.data_per_point['color'] = deepcopy(new_sft.streamlines)
# print('Generate points', time()-timer)

# timer = time()

# color_data = {}
# cmap = plt.get_cmap('jet')
# for i in np.unique(kmeans.labels_):
#     color_data[i] = np.array(cmap(i)[0:3]) * 255

# for i in range(len(new_sft)):
#     new_sft.data_per_point['color']._data[2*i:(2*i)+2] = color_data[kmeans.labels_[i]]
# print('Color points', time()-timer)