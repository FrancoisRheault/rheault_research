#!/usr/bin/env python

import argparse
import json
import logging
import os
import sys
import itertools

from dipy.io.streamline import save_tractogram, load_tractogram
from dipy.io.utils import is_header_compatible
import nibabel as nib
import numpy as np

from scilpy.io.image import get_data_as_mask
from scilpy.segment.streamlines import filter_grid_roi
from scilpy.utils.filenames import split_name_with_nii
from scilpy.tractanalysis.reproducibility_measures import (compute_bundle_adjacency_voxel,
                                                           compute_dice_voxel)
from dipy.align.imaffine import (transform_centers_of_mass,
                                 AffineMap)
from fury import window, actor, colormap
import matplotlib
import matplotlib.pyplot as plt
from nibabel.streamlines.array_sequence import ArraySequence

rois = sys.argv[2:-2]
sft = load_tractogram(sys.argv[1], 'same')

indices_list = []
for roi in rois:
    img = nib.load(roi)
    mask = get_data_as_mask(img)
    _, indices = filter_grid_roi(sft, mask, 'any',
                                 is_exclude=sys.argv[-2] == 'True')
    indices_list.extend(indices)
sft.to_rasmm()
unique_ind, count = np.unique(indices_list, return_counts=True)

count = count / len(rois)
color = plt.cm.get_cmap('plasma')(1-count)
sft.data_per_point['color'] = ArraySequence(sft.streamlines)

count = 0
coloring = []
for i in range(len(sft)):
    coloring += [color[i, 0:3] * 255] * len(sft.streamlines[i])
    count += len(sft.streamlines[i])

sft.data_per_point['color']._data = np.array(coloring)

sft.remove_invalid_streamlines()
save_tractogram(sft, sys.argv[-1])
