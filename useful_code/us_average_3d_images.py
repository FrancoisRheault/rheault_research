#!/usr/bin/env python

import sys
import nibabel as nib
import numpy as np
from scipy.ndimage.filters import gaussian_filter

"""""
    Give a list of image filenames (last one being the output filename),
    the script add them (grid-wise) and compute an average and save it.
    (Same dimensions, voxel-size, affine, etc.)
"""""

filename = sys.argv[1:len(sys.argv) - 2]
output = sys.argv[len(sys.argv) - 2]
blur = sys.argv[len(sys.argv) - 1]

first_img = nib.load(filename[0])
avg_volume = np.zeros(first_img.shape, dtype=np.float32)

print 'Averaging '+str(len(filename))+' images'
for i in filename:
    img = nib.load(i)
    if not np.allclose(first_img.shape, img.shape):
        raise ValueError('Different shape, FAIL !')

    if blur == 'y':
        avg_volume += gaussian_filter(img.get_data().astype(np.float32), 1, 0)
    else:
        avg_volume += img.get_data()

avg_volume /= len(filename)
nib.save(nib.Nifti1Image(avg_volume, first_img.affine), output)
