#!/usr/bin/env python

from PIL import Image
import os
import sys

"""""
    Give a list of image filenames (last one being the output filename),
    the script add them (grid-wise), the script will convert them all to
    jpeg (compress them) and you can play with the resolution

    #1 to #(n-1) : List of images
    #n : String to append to output file
"""""

filename = sys.argv[1:len(sys.argv) - 1]
output_append = sys.argv[len(sys.argv) - 1]
width = 900
heigth = 900

compress_filename = []
for i in filename:
    image = Image.open(i)
    out_name, out_ext = os.path.splitext(os.path.basename(i))
    image.save(out_name+'_c'+'.jpeg', quality=90)
    image = Image.open(out_name+'_c'+'.jpeg')
    image = image.resize((width, heigth), Image.LINEAR)
    image.save(out_name + output_append + '.jpeg', quality=90)
    print i
