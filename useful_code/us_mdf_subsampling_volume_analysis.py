#!/usr/bin/env python

import argparse
import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
import os
import random
import sys

from dipy.tracking.streamline import transform_streamlines
from scilpy.tractanalysis import compute_robust_tract_counts_map

filenames = sys.argv[1:len(sys.argv)]
trk_file = nib.streamlines.load(filenames[0], lazy_load=True)
original_length = trk_file.header['nb_streamlines']

length_streamlines_array = np.zeros((len(filenames),))
volume_mdf_array = np.zeros((len(filenames),))
volume_random_array = np.zeros((len(filenames),))
tractogram = nib.streamlines.load(filenames[0])
streamlines_ori = list(tractogram.streamlines)
transfo = tractogram.header["voxel_to_rasmm"]
streamlines_ori_vox = transform_streamlines(streamlines_ori,
                                        np.linalg.inv(transfo))
random.shuffle(streamlines_ori_vox)

for it, file in enumerate(filenames):
    tractogram = nib.streamlines.load(file)
    streamlines = list(tractogram.streamlines)
    length_streamlines_array[it] = len(streamlines)
    voxel_volume = np.prod(tractogram.header["voxel_sizes"][1:4])
    volume_dimensions = tractogram.header["dimensions"]
    transfo = tractogram.header["voxel_to_rasmm"]
    streamlines_vox = transform_streamlines(streamlines,
                                            np.linalg.inv(transfo))

    tdi = compute_robust_tract_counts_map(streamlines_vox,
                                          volume_dimensions)
    volume_a = np.count_nonzero(tdi) * voxel_volume
    volume_mdf_array[it] = volume_a

    tdi = compute_robust_tract_counts_map(streamlines_ori_vox[0:len(streamlines_vox)],
                                          volume_dimensions)
    volume_b = np.count_nonzero(tdi) * voxel_volume
    volume_random_array[it] = volume_b
    print len(streamlines_vox), int(volume_a), int(volume_b), int(volume_a - volume_b)

# Two subplots, the axes array is 1-d
f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(length_streamlines_array)
axarr[0].set_ylim(ymin=0)
axarr[1].plot(volume_mdf_array)
axarr[1].plot(volume_random_array)
axarr[1].set_ylim(ymin=0)

plt.show()
