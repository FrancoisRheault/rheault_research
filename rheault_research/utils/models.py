#!/usr/bin/env python
# encoding: utf-8

import os
import itertools
import time
import sys

import numpy as np
import nibabel as nib

from dipy.align.bundlemin import distance_matrix_mdf
from dipy.tracking.streamline import set_number_of_points, apply_affine
from dipy.segment.clustering import QuickBundles

from dipy.segment.clustering import qbx_and_merge
QBx_included = True

from dipy.align.streamlinear import (StreamlineLinearRegistration,
                                     BundleMinDistanceMetric)


def add_streamline(a, b, nb_points):
    a_sub = set_number_of_points(a, nb_points)
    b_sub = set_number_of_points(b, nb_points)

    direct_diff = a_sub - b_sub
    flip_diff = a_sub - b_sub[::-1]
    direct_dist = np.average(np.linalg.norm(direct_diff, axis=1))
    flip_dist = np.average(np.linalg.norm(flip_diff, axis=1))

    if direct_dist < flip_dist:
        return (a_sub + b_sub) / 2.0
    else:
        return (a_sub + b_sub[::-1]) / 2.0


def remove_similar_streamlines(streamlines, threshold=5, do_avg=False):
    if len(streamlines) == 1:
        return streamlines

    # Simple trick to make it faster than using 40-60 points
    sample_15_streamlines = set_number_of_points(streamlines, 15)
    distance_matrix = distance_matrix_mdf(sample_15_streamlines,
                                          sample_15_streamlines)

    current_id = 0
    avg_streamlines = []
    while True:
        indices = np.where(distance_matrix[current_id] < threshold)[0]

        pop_count = 0
        if do_avg:
            avg_streamline = sample_15_streamlines[current_id]
        if len(indices) > 1:
            for ind in indices:
                # Every streamlines similar to yourself (excluding yourself)
                # should be deleted from the set of desired streamlines
                if not current_id == ind:
                    kicked_out = streamlines.pop(ind-pop_count)
                    if do_avg:
                        avg_streamline = add_streamline(kicked_out,
                                                        avg_streamline, 15)
                    distance_matrix = np.delete(distance_matrix, ind-pop_count,
                                                axis=0)
                    distance_matrix = np.delete(distance_matrix, ind-pop_count,
                                                axis=1)
                    pop_count += 1
        if do_avg:
            avg_streamlines.append(avg_streamline)
        current_id += 1
        # Once you reach the end of the remaining streamlines
        if current_id >= len(streamlines):
            break

    if do_avg:
        return avg_streamlines
    else:
        return streamlines


def subsample_clusters(cluster_map, streamlines, min_distance,
                       min_cluster_size, average_streamlines=False,
                       verbose=False):
    output_streamlines = []
    # if verbose:
       # print 'Total streamlines in tractogram', len(streamlines)
       # print 'Total clusters in tractogram', len(cluster_map)

    # Each cluster is processed independently
    for i in range(len(cluster_map)):
        if len(cluster_map[i].indices) < min_cluster_size:
            continue
        cluster_streamlines = [streamlines[j] for j in cluster_map[i].indices]
        size_before = len(cluster_streamlines)

        chunk_count = 0
        leftover_size = size_before
        cluster_sub_streamlines = []
        # Prevent matrix above 1M (n*n)
        while leftover_size > 0:
            start_id = chunk_count * 1000
            stop_id = (chunk_count + 1) * 1000
            partial_sub_streamlines = remove_similar_streamlines(
                cluster_streamlines[start_id:stop_id],
                threshold=min_distance, do_avg=average_streamlines)

            # Add up the chunk results, update the loop values
            cluster_sub_streamlines.extend(partial_sub_streamlines)
            leftover_size -= 1000
            chunk_count += 1

        # Add up each cluster results
        output_streamlines.extend(cluster_sub_streamlines)

    return output_streamlines


def subsample_wrapper(streamlines, min_distance=4, cluster_thr=6,
                      min_cluster_size=2, average_streamlines=False,
                      verbose=False):
    min_cluster_size = max(min_cluster_size, 1)

    # QBx is highly recommended for any tractogram about 100k streamlines
    if QBx_included:
        thresholds = [40, 30, 20, cluster_thr]
        cluster_map = qbx_and_merge(streamlines, thresholds, verbose=False)
    else:
        qb = QuickBundles(threshold=cluster_thr)
        cluster_map = qb.cluster(streamlines)

    return subsample_clusters(cluster_map, streamlines, min_distance,
                              min_cluster_size, average_streamlines, verbose)


def interpolate_streamlines(streamlines, magnitude_of_interpolation=3.0):
    if len(streamlines) > 10000:
        ## print 'TOO MANY STREAMLINES !'
        return streamlines

    sample_streamlines = set_number_of_points(streamlines, 30)
    distance_matrix = distance_matrix_mdf(sample_streamlines, sample_streamlines)
    interpolated_streamlines = []
    tuple_dist = np.argwhere((distance_matrix < 4) & (distance_matrix <= 7))
    #print len(tuple_dist)
    for comb in tuple_dist:
        i, j = comb
        start = sample_streamlines[i]
        stop = sample_streamlines[j]

        if 4 < distance_matrix[i, j] <= 7:
            #magnitude_of_interpolation = (distance_matrix[i, j] / interps_step)
            step = 1/magnitude_of_interpolation

            for k in range(int(magnitude_of_interpolation)-1):
                dist = np.sum(np.linalg.norm(start - stop, axis=1))
                dist_flip = np.sum(np.linalg.norm(start - stop[::-1], axis=1))
                if dist_flip < dist:
                    interpolated_streamlines.append(
                        (k+1)*step*start + (1 - ((k+1)*step))*stop[::-1])
                else:
                    interpolated_streamlines.append(
                        (k+1)*step*start + (1 - ((k+1)*step))*stop)

    for i in sample_streamlines:
        interpolated_streamlines.append(i)

    return interpolated_streamlines


def find_closest_points(streamlines, centroids):
    closest_pts = []

    for i in streamlines:
        centroid = np.array(centroids)
        closest_pts.append(find_closest_point(i, centroid))

    return closest_pts


def find_closest_point(streamline, centroid):

    closest_pts = []
    for centroid_pos in range(len(centroid)):
        current_pt_min = 1000
        current_id_min = 1000

        for streamline_pos in range(len(streamline)):
            current_dist = streamline[streamline_pos] - centroid[centroid_pos]
            current_dist = np.linalg.norm(current_dist)
            #print centroid_pos, streamline_pos, current_dist
            if current_dist < current_pt_min and current_dist < 15:
                current_pt_min = current_dist
                current_id_min = streamline_pos

        closest_pts.append(current_id_min)

    return closest_pts


def is_thigh_bundles(centroid, bundle, avg, std):

    distance_matrix = distance_matrix_mdf(bundle, centroid)
    ## print "Average dist. to centroid : ", np.average(distance_matrix)
    ## print "Standard deviation of distance to centroid : ", np.std(distance_matrix)

    return np.average(distance_matrix) < avg and np.std(distance_matrix) < std


def distance_between_bundles(bundle_a, bundle_b):

    sum_of_matrix = np.sum(np.min(distance_matrix_mdf(bundle_a, bundle_b), axis=1))
    nb_of_point_wise = 20.0 * len(bundle_a) * len(bundle_b)

    return sum_of_matrix / nb_of_point_wise


def maximize_distance_between_models(models_dir, models_list, models_basename):
    model_bundles_dict = {}
    subj_tag = open(models_list).read().split('\n')
    del subj_tag[-1]

    metric = BundleMinDistanceMetric()
    bounds = [(-30, 30), (-30, 30), (-30, 30),
              (-45, 45), (-45, 45), (-45, 45), (0.8, 1.2)]
    slr = StreamlineLinearRegistration(metric=metric, x0='similarity',
                                       bounds=bounds)
    for tag in subj_tag:
        trk_file = nib.streamlines.load(
            os.path.join(models_dir, tag, models_basename+'.trk'))
        current_model = set_number_of_points(trk_file.streamlines, 20)

        if not tag == subj_tag[0]:
            ## print 'Registration of '+tag+' to '+subj_tag[0]+'...'
            slm = slr.optimize(model_bundles_dict[subj_tag[0]], current_model)
            transf_streamlines = apply_affine(slm.matrix, current_model)
            model_bundles_dict[tag] = transf_streamlines
        else:
            model_bundles_dict[tag] = current_model

    vote_dict = {}
    for i in model_bundles_dict.keys():
        vote_dict[i] = 0
    #print
    for i in range(len(subj_tag)):
        model_tag = [subj_tag[i]]
       # print 'Init. greedy algo. with '+subj_tag[i]
        it = 0
        while it < 6:
            remaining_tag = list(set(subj_tag) - set(model_tag))
            potential_choice_value = np.zeros((len(remaining_tag)))
            for current_it in range(len(remaining_tag)):
                potential_choice_value[current_it] = \
                    get_distance_function(model_bundles_dict, remaining_tag,
                                          model_tag, remaining_tag[current_it])

            best_choice = np.argmin(potential_choice_value)
            model_tag.append(remaining_tag[best_choice])
            it += 1

        for result in model_tag:
            vote_dict[result] += 1
    #print
    #print vote_dict


def get_distance_function(dictionary, remaining_tag, model_tag, current_tag):

    current_to_remaining = np.zeros((len(remaining_tag)))
    current_to_model = np.zeros((len(model_tag)))

    for row in range(len(remaining_tag)):
        bundle_distance = distance_between_bundles(
            dictionary[current_tag],
            dictionary[remaining_tag[row]])
       # print current_tag, remaining_tag[row]
       # print bundle_distance
        current_to_remaining[row] = bundle_distance

    for row in range(len(model_tag)):
        bundle_distance = distance_between_bundles(
            dictionary[current_tag],
            dictionary[model_tag[row]])
        current_to_model[row] = bundle_distance

    value = (0.4 * np.average(current_to_remaining)) - \
            (0.6 * np.average(current_to_model))

    return value
