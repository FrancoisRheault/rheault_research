import numpy as np

from dipy.viz import fvtk
from dipy.tracking.streamline import transform_streamlines

def render_bundles(static, moving, linewidth=1, tubes=False,
                   opacity=1.0, fname=None):

    # Rendering of the current results using ftvk
    ren = fvtk.ren()
    ren.clear()
    ren.SetBackground(1, 1, 1)

    if tubes:
        static_actor = fvtk.streamtube(static, fvtk.colors.red,
                                       linewidth=linewidth, opacity=opacity)
        moving_actor = fvtk.streamtube(moving, fvtk.colors.blue,
                                       linewidth=linewidth, opacity=opacity)
    else:
        static_actor = fvtk.line(static, fvtk.colors.red,
                                 linewidth=linewidth, opacity=opacity)
        moving_actor = fvtk.line(moving, fvtk.colors.blue,
                                 linewidth=linewidth, opacity=opacity)

    fvtk.add(ren, static_actor)
    fvtk.add(ren, moving_actor)

    fvtk.add(ren, fvtk.axes(scale=(20, 20, 20)))
    fvtk.show(ren, size=(900, 900))

    if fname is not None:
        fvtk.record(ren, size=(900, 900), out_path=fname)


def display_recobundles(model_bundle, recognized_bundle, rb):
    mat2 = np.eye(4)
    mat2[:3, 3] = np.array([75, 75, 0])

    # Display the current result, useful to quickly verify the set of
    # parameters a, b, c without saving everything
    render_bundles(rb.model_centroids, rb.centroids, linewidth=2, tubes=False)
    render_bundles(rb.model_centroids, rb.rtransf_centroids, tubes=False)
    if len(recognized_bundle) > 0:
        render_bundles(model_bundle, recognized_bundle, tubes=False)
        render_bundles(transform_streamlines(model_bundle, mat2),
                       recognized_bundle, tubes=False)