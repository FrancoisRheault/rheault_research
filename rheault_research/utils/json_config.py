#!/usr/bin/env python
# encoding: utf-8

import os
import json


def verify_config_json(json, output_dir):

    json["models_parameters"]["models_directory"] =\
        os.path.expanduser(json["models_parameters"]["models_directory"])

    output_dir += json["script_parameters"]["output_directory"]
    json["script_parameters"]["output_directory"] =\
        os.path.expanduser(output_dir)

    transformation_filename = json["recobundles_parameters"]["transformation_to_first_model"]
    if not os.path.isfile(transformation_filename):
        print 'Provided transformation file does not exist'
        return False

    for group in json["bundles_groupname"]:
        if group not in json["bundles_specific_parameters"]:
            print group+' groupname is not found in bundles_specific_parameters'
            return False

    nb_parameters = json["number_of_parameters"]
    if not len(json["recobundles_parameters"]["subject_tractogram_clustering_threshold"]) == nb_parameters:
            print 'subject_tractogram_clustering_threshold'+\
                  'have the wrong number of parameters ('+\
                  str(nb_parameters)+')'
            return False

    params = json["bundles_specific_parameters"]
    for bundle in json["bundles_groupname"]:
        if not nb_parameters == len(params[bundle]["model_clustering_threshold"]):
            print 'model_clustering_threshold for '+bundle+\
                  'have the wrong number of parameters ('+\
                  str(nb_parameters)+')'
            return False
        if not nb_parameters == len(params[bundle]["pruning_threshold"]):
            print 'pruning_threshold for '+bundle+\
                  'have the wrong number of parameters ('+\
                  str(nb_parameters)+')'
            return False

        params[bundle]["sub_bundles_config"] = os.path.expanduser(params[bundle]["sub_bundles_config"])

    subj_tag = json["models_parameters"]["models_list"]
    total_number_of_run = json["script_parameters"]["number_of_execution"]*len(subj_tag)
    if total_number_of_run < json["script_parameters"]["minimal_vote"]:
        print 'Minimal vote must be below the total number of run'
        return False

    return True
