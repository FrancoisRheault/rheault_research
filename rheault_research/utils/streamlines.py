import numpy as np
import nibabel as nib
from scipy.ndimage.filters import gaussian_filter1d
from scipy.interpolate import splprep, splev

from dipy.align.streamlinear import StreamlineLinearRegistration
from dipy.tracking.metrics import length
from dipy.tracking.streamline import (set_number_of_points,
                                      select_random_set_of_streamlines,
                                      transform_streamlines)
from dipy.tracking.streamlinespeed import compress_streamlines


# from scilpy.io.streamlines import save_trk_from_voxel_space, load_trk_in_voxel_space


def read_next(iterator, n):
    """Reads and returns 'n' next streamlines (or less if not enough left
    to read) from the current iterator's position."""
    streamlines = []
    for _ in range(n):
        try:
            streamlines.append(next(iterator))
        except StopIteration:
            break
    return streamlines


def smooth_line_gaussian(streamline, sigma, error_rate=0):
    if sigma < 0.001:
        ValueError('Cant have a 0 sigma with gaussian')

    nb_points = int(length(streamline)*10)
    if nb_points < 2:
        nb_points = 2
    sample_streamline = set_number_of_points(streamline, nb_points)

    if sigma != 0:
        x, y, z = sample_streamline.T
        x3 = gaussian_filter1d(x, sigma)
        y3 = gaussian_filter1d(y, sigma)
        z3 = gaussian_filter1d(z, sigma)
        smoothed_streamlines = np.asarray([x3, y3, z3]).T

        # Ensure first and last point remain the same
        smoothed_streamlines[0] = streamline[0]
        smoothed_streamlines[len(smoothed_streamlines)-1] = \
            streamline[len(streamline)-1]

        if error_rate > 0.00001:
            compressed_streamlines = compress_streamlines(smoothed_streamlines,
                                                          error_rate)
            return compressed_streamlines
        return smoothed_streamlines


def smooth_line_spline(streamline, sigma, error_rate):
    if sigma < 0.001:
        ValueError('Cant have a 0 sigma with spline')

    nb_points = int(length(streamline)*1)
    if nb_points < 2:
        nb_points = 2
    tck, u = splprep(streamline.T, s=sigma)
    smoothed_streamlines = splev(np.linspace(0, 1, nb_points), tck)
    smoothed_streamlines = np.squeeze(np.asarray([smoothed_streamlines]).T)

    # Ensure first and last point remain the same
    smoothed_streamlines[0] = streamline[0]
    smoothed_streamlines[len(smoothed_streamlines)-1] = \
        streamline[len(streamline)-1]

    if error_rate > 0.0001:
        return compress_streamlines(smoothed_streamlines,
                                    error_rate)
    else:
        return set_number_of_points(smoothed_streamlines, 99)


def get_shift_vector(header):
    dims = header[nib.streamlines.Field.DIMENSIONS]
    voxel_dim = header[nib.streamlines.Field.VOXEL_SIZES]
    shift_vector = -1.0 * (np.array(dims) * voxel_dim / 2.0)

    return shift_vector


def flip_streamlines(tract_filename):
    in_tractogram = nib.streamlines.load(tract_filename)
    transfo = in_tractogram.header["voxel_to_rasmm"]
    streamlines = list(in_tractogram.streamlines)
    streamline_vox = load_in_voxel_space(streamlines, transfo)

    flip_vector = [-1, 1, 1]
    shift_vector = get_shift_vector(in_tractogram.header)

    flipped_tracts = []
    for tract in streamline_vox:
        mod_tract = tract + shift_vector
        mod_tract *= flip_vector
        mod_tract -= shift_vector
        flipped_tracts.append(mod_tract)

    return streamlines, transform_streamlines(flipped_tracts, transfo)


def align_bundles(bundle_1, bundle_2, num_points=30, num_threads=4,
                  num_streamlines=200, num_iter=10):
    bundle_1_sub = set_number_of_points(bundle_1, num_points)
    bundle_2_sub = set_number_of_points(bundle_2, num_points)
    bounds = [(-10, 10), (-10, 10), (-10, 10),
              (-10, 10), (-10, 10), (-10, 10)]
    slr = StreamlineLinearRegistration(bounds=bounds,
                                       options={'maxiter': num_iter},
                                       method='L-BFGS-B',
                                       num_threads=num_threads)
    slr_opt = slr.optimize(static=select_random_set_of_streamlines(bundle_1_sub,
                                                                   num_streamlines),
                           moving=select_random_set_of_streamlines(bundle_2_sub,
                                                                   num_streamlines))
    return slr_opt.transform(bundle_2)