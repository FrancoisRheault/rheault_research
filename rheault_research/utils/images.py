#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import scipy.ndimage as ndi

def morphological_clean_blob(array, dilation_it=1, erosion_it=2):
    binary_structure = np.ones((3, 3, 3))

    cleaned_array = ndi.morphology.binary_dilation(array,
                                                   structure=binary_structure,
                                                   iterations=dilation_it)
    cleaned_array = ndi.morphology.binary_erosion(cleaned_array,
                                                  structure=binary_structure,
                                                  iterations=erosion_it)
    cleaned_array = ndi.morphology.binary_dilation(cleaned_array,
                                                   structure=binary_structure,
                                                   iterations=dilation_it)
    return cleaned_array


def labelize_endpoints(array):
    label_objects, nb_labels = ndi.label(array)
    sizes = np.bincount(label_objects.ravel())

    return label_objects, sizes


def floodfill_using_label(original, cleaned):
    ori_labels, ori_sizes = labelize_endpoints(original)
    clean_labels, clean_sizes = labelize_endpoints(cleaned)

    ori_filled = original.copy()
    label_to_keep = []
    for i in range(len(clean_sizes)):
        avg = np.median(ori_labels[clean_labels == i])
        if not np.isnan(avg) and i != 0:
            label_to_keep.append(int(round(avg)))

    for i in range(len(ori_sizes)):
        if i not in label_to_keep:
            ori_filled[ori_labels == i] = 0

    return ori_filled