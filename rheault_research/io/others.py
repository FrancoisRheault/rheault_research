#! /usr/bin/env python
# -*- coding: utf-8 -*-

import difflib
import os
import xml.etree.ElementTree as ET


# In a list provided from REGEX, find the longest common part of the string
def find_common_name(filenames):
    string1 = os.path.splitext(filenames[0])[0]
    string2 = os.path.splitext(filenames[-1])[0]
    blocks = difflib.SequenceMatcher(None, string1,
                                     string2).get_matching_blocks()
    matching_block = []
    for i, match in enumerate(blocks):
        if match.size > 15:
            matching_block.append(string1[match.a: match.a + match.size])
            print matching_block[-1]

    return matching_block


# From a filename (dirname + basename) and a common_substring
# returns an unique filename to save
def get_unique_name(filename, common_substring=None):
    filename, _ = os.path.splitext(filename)

    if common_substring:
        for substring in common_substring:
            filename = filename.replace(substring, '')

    filename_no_dir = filename.replace('/', '_')

    if filename_no_dir[-1] == '_':
        filename_no_dir.pop(-1)

    return filename_no_dir


def find_in_list(in_list, item):
    for i, j in enumerate(in_list):
        if j == item:
            return i

    return None


def save_mb_bdo(in_filename, center, out_filename):
    tree = ET.parse(in_filename)
    root = tree.getroot()
    center_tag = root.find('origin')
    flip = [-1, -1, 1]
    center_tag.attrib['x'] = str(flip[0] * float(center[0]))
    center_tag.attrib['y'] = str(flip[1] * float(center[1]))
    center_tag.attrib['z'] = str(flip[2] * float(center[2]))

    tree.write(out_filename)