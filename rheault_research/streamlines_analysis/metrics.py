import copy

from dipy.tracking.distances import bundles_distances_mdf
from dipy.tracking.streamline import set_number_of_points
import numpy as np
import nibabel as nib
from scipy import spatial
from sklearn.metrics import cohen_kappa_score

from scilpy.utils.streamlines import perform_streamlines_operation
from scilpy.utils.streamlines import subtraction, intersection, union

def labelize_streamlines(bundle_a, bundle_b):
    streamlines_labels = []
    it = 0
    for (j, b) in enumerate(bundle_b):
        was_labeled = False
        for (i, a) in enumerate(bundle_a):

            if a.shape[0] - b.shape[0] < 5:
                continue
            first_pt = np.sum((b[0] - a[0])**2)
            if first_pt > 1:
                continue
            last_pt = np.sum((b[-1] - a[-1])**2)
            if last_pt > 1:
                continue

            # This mean the criteria are met,
            # after that we can identify the next streamlines
            streamlines_labels.append(i)
            was_labeled = True
            break
        if not was_labeled:
            streamlines_labels.append(-1)
        it += 1

    return list(set(streamlines_labels))