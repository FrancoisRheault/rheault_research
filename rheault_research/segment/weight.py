import matplotlib.pyplot as plt
from scipy import signal


def roll_zeropad(a, shift, axis=None):
    a = np.asanyarray(a)
    if shift == 0:
        return a
    if axis is None:
        n = a.size
        reshape = True
    else:
        n = a.shape[axis]
        reshape = False

    if np.abs(shift) > n:
        res = np.zeros_like(a)
    elif shift < 0:
        shift += n
        zeros = np.zeros_like(a.take(np.arange(n-shift), axis))
        res = np.concatenate((a.take(np.arange(n-shift, n), axis), zeros), axis)
    else:
        zeros = np.zeros_like(a.take(np.arange(n-shift, n), axis))
        res = np.concatenate((zeros, a.take(np.arange(n-shift), axis)), axis)

    if reshape:
        return res.reshape(a.shape)
    else:
        return res


def generate_weight(position_percentage, nb_points):
    weight_value = []
    for it in range(len(position_percentage)):
        x = np.arange(nb_points, dtype=np.float64)
        peak_position = np.asarray(position_percentage[it]*nb_points,
                                   dtype=np.int)
        if peak_position.size == 1:
            peak_position = peak_position.reshape((1,))

        if len(peak_position) > 0:
            window = signal.gaussian(nb_points, std=0.05*nb_points)
            x[0] += 0.01

            y = np.zeros(nb_points)
            for i in peak_position:
                y += roll_zeropad(window / len(peak_position), int(-1*nb_points/2.0 + i))
            max_weigth_peak = np.exp(-0.9/x)
            max_weigth_peak = max_weigth_peak / np.max(max_weigth_peak) * 0.75

            max_pos = min(len(max_weigth_peak)-1, len(peak_position))
            y = (y / np.sum(y)) * max_weigth_peak[max_pos]
            y += (1 - np.sum(y)) / nb_points
        else:
            y = np.ones((nb_points)) / nb_points
        plt.plot(x, y, 'or-')

        plt.text(1, 0.2, 'total value : '+str(np.sum(y)))
        plt.axis([0, nb_points, 0, 0.5])
        plt.grid(True)
        # plt.show()
        weight_value.append(y)

    return weight_value


def generate_hard_threshold(position_percentage, nb_points):
    weight_value = []
    for it in range(len(position_percentage)):
        x = np.arange(nb_points, dtype=np.float64)
        peak_position = np.asarray(position_percentage[it]*nb_points,
                                   dtype=np.int)
        if peak_position.size == 1:
            peak_position = peak_position.reshape((1,))

        if len(peak_position) > 0:
            window = signal.gaussian(nb_points, std=0.05*nb_points)
            x[0] += 0.01

            y = np.zeros(nb_points)
            for i in peak_position:
                y[i] = 1
        plt.plot(x, y, 'or-')

        plt.text(1, 0.2, 'total value : '+str(np.sum(y)))
        plt.axis([0, nb_points, 0, 0.5])
        plt.grid(True)
        # plt.show()
        weight_value.append(y)

    return weight_value